package com.cc.jsl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaSpringListenerApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaSpringListenerApplication.class, args);
    }

}
