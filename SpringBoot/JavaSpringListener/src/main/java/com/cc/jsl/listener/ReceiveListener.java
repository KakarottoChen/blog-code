package com.cc.jsl.listener;

import com.cc.jsl.event.EmailEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * <p>事件监听者（Event Listener）</p>
 *
 * @author CC
 * @since 2023/10/10
 */
@Component
public class ReceiveListener {

    /**
     * 打电话 - 接收普通JavaBean参数
     */
    @Async
    @EventListener
    public void sendCall(String msg) {
        //发送邮件逻辑
        System.out.println("打电话！-普通-> " + msg);
    }

    /**
     * 发送邮件 - 接收字符串参数
     */
    @Async
    @EventListener
    public void sendEmail(String msg) {
        //发送邮件逻辑
        System.out.println("发送邮件！-普通-> " + msg);
    }

    /**
     * 发送邮件 - 接收唯一事件
     */
    @Async
    @EventListener(EmailEvent.class)
    public void sendEmail(EmailEvent emailEvent) {
        //发送邮件逻辑
        System.out.println("发送邮件！-事件-> " + emailEvent);
    }
}
