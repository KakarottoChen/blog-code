package com.cc.jsl.event;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

/**
 * <p>发邮件专属的唯一事件</p>
 * <p>需要实现set方法</p>
 *
 * @author CC
 * @since 2023/10/10
 */
@Getter
@Setter
public class EmailEvent extends ApplicationEvent {

    /**
     * 参数1
     */
    private String name;

    /**
     * 参数2
     */
    private Integer age;

    public EmailEvent(Object source, String name, Integer age) {
        super(source);
        this.name = name;
        this.age = age;
    }

}
