package com.cc.jschdemo.web.controller;

import com.cc.jschdemo.utils.JSchUtil;
import com.jcraft.jsch.ChannelExec;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.util.Arrays;

/**
 * <p></p>
 *
 * @author CC
 * @since 2023/11/8
 */
@RestController
@RequestMapping("/jsch")
public class JSchController {

    @Resource
    private JSchUtil jSchUtil;

    /** <p>执行命令<p>
     **/
    @GetMapping
    public String executeCommand() {
        //登陆（默认只连接5分钟，5分钟后销毁）
        jSchUtil.loginLinux("root", "", "", 22);
        //一、执行命令
        String mkdir = jSchUtil.executeCommand("mkdir ccc");
        String docker = jSchUtil.executeCommand("docker");
        String dockerPs = jSchUtil.executeCommand("docker ps");

        System.out.println(mkdir);
        System.out.println(docker);
        System.out.println(dockerPs);

        //二、执行shell脚本（可以改造成传入的shell脚步）
        /*try {
            jSchUtil.execCmdByShell(Arrays.asList("cd /","ll"));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }*/

//        String ls = root.ls("/");

        //执行完，关闭连接
        jSchUtil.closeAll();

        return docker;
    }

    /** <p>执行命令<p>
     **/
    @PostMapping
    public String execCmdByShell() {
        //登陆（默认只连接5分钟，5分钟后销毁）
        jSchUtil.loginLinux("root", "", "", 22);
        //二、执行shell脚本（可以改造成传入的shell脚步）
        jSchUtil.execCmdByShell(Arrays.asList("cd /", "ll" , "cd cc/", "mkdir ccccc222", "ll"));
        //执行完，关闭连接
        jSchUtil.closeAll();

        return "docker";
    }

    //下载普通服务器的文件
    @PutMapping
    public void downloadOtherFile(HttpServletResponse response) {
        //登陆（默认只连接5分钟，5分钟后销毁）
        jSchUtil.loginLinux("", "", "", 22);
        //下载文件
        jSchUtil.downloadOtherFile(
                "/dev/libbb/studio-3t-x64.zip",
                "studio-3t-x64.zip",
                response
        );
        //执行完，关闭连接
        jSchUtil.closeAll();
    }

    //下载云服务器的文件（阿里云为例）
    @PutMapping("/downloadCloudServerFile")
    public String downloadCloudServerFile(HttpServletResponse response) {
        //登陆（默认只连接5分钟，5分钟后销毁）
        jSchUtil.loginLinux("root", "", "", 22);
        //下载云服务器的文件
        jSchUtil.downloadCloudServerFile(
                "/cc/phantomjs-2.1.1-windows.zip",
                "phantomjs-2.1.1-windows.zip",
                response
        );
        //执行完，关闭连接
        jSchUtil.closeAll();

        return "docker";
    }



}
