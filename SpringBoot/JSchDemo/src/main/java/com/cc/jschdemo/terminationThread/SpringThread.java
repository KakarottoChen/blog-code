package com.cc.jschdemo.terminationThread;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.stereotype.Component;

/**
 * <p>注入spring的线程</p>
 *
 * @author --
 * @since 2023/11/24
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Component
public class SpringThread extends Thread{

    //1可以注入Spring的Bean，然后直接使用
//    @Resource
//    private IBasicInterruptService basicInterruptService;

    //2可以直接传入执行任务的参数（可多个）
    private String footwork;

    /**
     * 具体要执行的任务
     */
    @Override
    public void run() {
        try {
            //步奏1
            Thread.sleep(1000);
            System.out.printf("完成：%s 1 %n", footwork);

            //步奏2（模仿阻塞）
            while (true) {
                Thread.sleep(1000);
                System.out.printf("完成：%s 2 %n", footwork);
            }
        }catch (InterruptedException e) {
            e.printStackTrace();
            //捕获异常：InterruptedException，然后打断
            //☆☆打断，终止当前线程☆☆
            Thread.currentThread().interrupt();
        }
    }


}
