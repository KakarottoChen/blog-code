package com.cc.jschdemo.terminationThread;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.CompletableFuture;

/**
 * <p></p>
 *
 * @author --
 * @since 2023/11/25
 */
@RestController
@RequestMapping("/completableFuture")
public class CompletableFutureThread {

    //这里使用标记打断的话，每个CompletableFuture都需要一个标记，可以（CompletableFuture和标记）一起缓存下来。
    boolean flagTask1 = false;
    boolean flagTask2 = false;

    //模仿任务1线程，也可以是：Runnable
    Thread task1 = new Thread(() -> {
        try {
            //模仿线程无限阻塞（while (true)）
            while (true) {
                System.out.println("任务1执行...");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }

                //检查终止状态，如果终止就报错。true已中断；false未中断
                // 这里获取到的是：CompletableFuture启动线程池的线程池的线程。就是执行task1的线程
                boolean interrupted = Thread.currentThread().isInterrupted();
                // 所以interrupted不能用来终止task1

                //这里使用标记来终止task1（可以根据业务，在需要终止的地方设置这个打断。）
                if (flagTask1) {
                    //打断：CompletableFuture启动线程池的线程池的线程
                    Thread.currentThread().interrupt();
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    });

    //任务2
    Thread task2 = new Thread(() -> {
        try {
            //模仿线程阻塞
            while (true) {
                System.out.println("任务2执行...");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }

                //这里使用标记来终止task1
                if (flagTask2) {
                    Thread.currentThread().interrupt();
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    });

    //缓存CompletableFuture
    CompletableFuture<Void> future1;
    CompletableFuture<Void> future2;

    //启动任务
    @GetMapping
    public void test08()throws Exception{
        //实际使用中，可以使用循环添加CompletableFuture，然后记录下future（缓存），在要中断的逻辑中调用cancel
        //1开启task任务。这里实际是CompletableFuture调用ForkJoinPool线程池启动一个线程去执行task1任务。
        //要想终止任务，要终止CompletableFuture调用ForkJoinPool线程池创建的线程，而非task1线程。
        future1 = CompletableFuture.runAsync(task1);
        future2 = CompletableFuture.runAsync(task2);
    }

    //终止任务
    @PostMapping
    public void stop() {
        //3取消任务
        // 参数true表示尝试中断任务执行
        // cancel方法会尝试取消任务的执行，参数true表示尝试中断任务执行。
        // 成功取消返回true，否则返回false。
        // 需要注意的是，cancel方法并不会直接中断正在执行的线程，而是会尝试取消任务的执行，
        // 具体是否成功取决于任务的实现。

        //终止方式1：使用cancel终止：无法终止task1
        boolean cancel1 = future1.cancel(Boolean.TRUE);
        System.out.printf("终止方式1-执行，结果：%s%n", cancel1);
        //终止方式2：打断线程task1：无法终止task1
        task1.interrupt();
        System.out.println("终止方式2-执行");

        //睡5s，让任务跑5s，看终止方式1、2是否能终止，测试结果为：不能终止
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        //终止方式3：直接标记打断。CompletableFuture调用ForkJoinPool线程池生成的线程
        // 真正的终止task1
        flagTask1 = true;
        System.out.println("终止方式3-执行");
    }


}
