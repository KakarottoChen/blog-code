package com.cc.std;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/** 启动类
 * @since 2023/3/3 0003
 * @author CC
 **/
@SpringBootApplication
@MapperScan("com.cc.std.mapper")
public class SaTokenDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SaTokenDemoApplication.class, args);
    }

}
