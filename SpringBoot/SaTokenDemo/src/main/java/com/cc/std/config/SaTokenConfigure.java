package com.cc.std.config;

import cn.dev33.satoken.interceptor.SaInterceptor;
import cn.dev33.satoken.router.SaRouter;
import cn.dev33.satoken.stp.StpUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** 配置类
 * @since 2023/3/7 0007
 * @author CC
 **/
@Configuration
public class SaTokenConfigure implements WebMvcConfigurer {

    /** 注册 Sa-Token 拦截器，打开注解式鉴权功能
     * @param registry 注册表
     * @since 2023/3/7 0007
     * @author CC
     **/
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //放行的地址
        List<String> notMatch = new ArrayList<>();
        notMatch.add("/user/login");

        // 注册 Sa-Token 拦截器，打开注解式鉴权功能
        registry.addInterceptor(new SaInterceptor(handle -> {
                            //拦截所有 /** 请求
                    SaRouter.match("/**")
                            //放行的接口
                            .notMatch(notMatch)
                            //验证是否登陆（取反）：是登陆true
                            .match(!StpUtil.isLogin())
                            //上面返回false，继续执行这个back（可以是错误json）
                            .back(this.getResultMsg());
                }))
                .addPathPatterns("/**");
    }

    /** 获取返回的msg
     */
    private String getResultMsg() {
        return "{\"msg\":\"未登陆，请先登陆！\",\"status\":\"500\"}";
    }

    /** 解决跨域
     * @return org.springframework.web.filter.CorsFilter
     * @since 2023/3/7 0007
     * @author CC
     **/
    @Bean
    public CorsFilter corsFilter() {

        //创建CorsConfiguration对象后添加配置
        CorsConfiguration config = new CorsConfiguration();
        //设置放行哪些原始域
        config.addAllowedOriginPattern("*");
        //放行哪些原始请求头部信息
        config.addAllowedHeader("*");
        //暴露哪些头部信息
        config.addExposedHeader("*");
        //放行哪些请求方式
        //get
        config.addAllowedMethod("GET");
        //put
        config.addAllowedMethod("PUT");
        //post
        config.addAllowedMethod("POST");
        //delete
        config.addAllowedMethod("DELETE");
        //corsConfig.addAllowedMethod("*");     //放行全部请求

        //是否发送Cookie
        config.setAllowCredentials(true);

        //2. 添加映射路径
        UrlBasedCorsConfigurationSource corsConfigurationSource =
                new UrlBasedCorsConfigurationSource();
        corsConfigurationSource.registerCorsConfiguration("/**", config);
        //返回CorsFilter
        return new CorsFilter(corsConfigurationSource);
    }
}