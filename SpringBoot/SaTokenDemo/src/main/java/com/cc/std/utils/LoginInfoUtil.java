package com.cc.std.utils;

import cn.dev33.satoken.session.SaSession;
import cn.dev33.satoken.stp.StpUtil;
import com.cc.std.vo.LoginInfoVo;
import com.google.common.collect.Maps;

import java.util.Map;

/** 获取登陆信息的工具类
 * @author CC
 * @since 2023/3/7 0007
 */
public class LoginInfoUtil {

    /** 获取登陆信息总方法
     * @return 返回登陆信息
     */
    private static LoginInfoVo getLoginInfoVo(){
        //获取登陆时传入的登陆id（这里是用户id）
        String loginId = (String) StpUtil.getLoginId();

        SaSession session = StpUtil.getSession();

        //根据用户id，获取自己的session信息，并格式化成 LoginInfoVo
        return session.getModel(loginId, LoginInfoVo.class);
    }

    /**
     * 获取当前登陆用户的 id
     */
    public static String getUserId(){
        return getLoginInfoVo().getId();
    }

    /**
     * 获取当前登陆用户的 Username
     */
    public static String getUsername(){
        return getLoginInfoVo().getUsername();
    }

    /**
     * 获取当前登陆用户的 Password
     */
    public static String getPassword(){
        return getLoginInfoVo().getPassword();
    }

    /**
     * 获取当前登陆用户的 Name
     */
    public static String getName(){
        return getLoginInfoVo().getName();
    }

    /**
     * 获取当前登陆用户的 Token
     */
    public static Map<String,String> getToken(){
        String tokenName = getLoginInfoVo().getTokenName();
        String tokenValue = getLoginInfoVo().getTokenValue();
        Map<String,String> map = Maps.newHashMap();
        map.put(tokenName,tokenValue);
        return map;
    }

}
