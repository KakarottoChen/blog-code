package com.cc.std.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cc.std.entity.StUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author CC
 * @since 2023/3/6 0006
 */
@Mapper
public interface StUserMapper extends BaseMapper<StUser> {



}
