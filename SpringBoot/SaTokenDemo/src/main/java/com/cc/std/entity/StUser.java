package com.cc.std.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/** 用户表
 * @author CC
 * @since 2023/3/6 0006
 */
@Data
@TableName("T_USER")
public class StUser implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId("ID")
    private String id;

    /**
     * 名字
     */
    @TableField("NAME")
    private String name;

    /**
     * 名字
     */
    @TableField("USERNAME")
    private String username;

    /**
     * 名字
     */
    @TableField("PASSWORD")
    private String password;

}
