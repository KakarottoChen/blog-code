package com.cc.std.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cc.std.entity.StUser;
import com.cc.std.req.LoginReq;
import com.cc.std.vo.LoginInfoVo;

/**
 * @author CC
 * @since 2023/3/6 0006
 */
public interface IStUserService extends IService<StUser> {



    LoginInfoVo login(LoginReq req);

}
