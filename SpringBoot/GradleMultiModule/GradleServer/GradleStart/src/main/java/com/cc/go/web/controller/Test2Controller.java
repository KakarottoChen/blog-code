package com.cc.go.web.controller;

import cn.hutool.core.util.StrUtil;
import com.cc.gc.entity.one.Entity1;
import com.cc.gu.util1.U1;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

/**
 * <p></p>
 *
 * @author CC
 * @since 2023/9/20
 */
@RequestMapping("/testStart")
@RestController
public class Test2Controller {

    @GetMapping
    public String qqqq(){
        //1证明：GradleOne模块被依赖成功
        TestController cc = new TestController();
        System.out.println("我是GradleOne模块");

        //证明GradleCommon模块是被引用了的 —— 因为GradleOne模块引入了GradleCommon
        Entity1 entity1 = new Entity1();
        entity1.setId1("GradleCommon模块");
        System.out.println("我是" + entity1.getId1());
        String sub = StrUtil.sub("", 1, 2);

        //证明GradleUtil模块是被引用了的 —— 因为GradleOne模块引入了GradleUtil
        String s = U1.as();
        boolean notBlank = StringUtils.isNotBlank(s);
        System.out.println(s);

        //2证明：GradleTwo模块被依赖成功
        Test2Controller test2Controller = new Test2Controller();
        System.out.println("我是GradleTwo模块");

        return "我是GradleStart模块";
    }


}
