package com.cc.go.web.controller;

import cn.hutool.core.util.StrUtil;
import com.cc.gc.entity.one.Entity1;
import com.cc.gu.util1.U1;
import lombok.Data;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p></p>
 *
 * @author CC
 * @since 2023/9/20
 */
@RequestMapping("/testOne")
@RestController
//证明：可以使用GradleUtil模块的Lombok包
@Data
public class TestController {

    @GetMapping
    public String get1(){
        //证明：GradleUtil模块被依赖成功
        String s = U1.as();
        System.out.println(s);

        //证明：GradleCommon模块被依赖成功
        Entity1 entity1 = new Entity1();
        entity1.setId1("我是GradleCommo模块！");
        //证明：可以使用GradleCommon模块的依赖：hutool工具类
        String sub = StrUtil.sub("", 1, 2);
        System.out.println(entity1.getId1());

        return "我是GradleOne模块！";
    }
}