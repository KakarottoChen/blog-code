package com.cc.go.web.controller;

import cn.hutool.core.net.NetUtil;
import cn.hutool.core.util.StrUtil;
import com.cc.gc.entity.one.Entity1;
import com.cc.gu.util1.U1;
import lombok.Data;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p></p>
 *
 * @author CC
 * @since 2023/9/20
 */
@RestController
@RequestMapping("/testTto")
@Data
public class Test2Controller {

    @GetMapping("/tt2")
    public String get2(){
        //证明：GradleUtil模块被依赖成功
        String s = U1.as();
        System.out.println(s);

        //证明：GradleCommon模块被依赖成功，且可以使用hutool工具
        Entity1 entity1 = new Entity1();
        entity1.setId1("我是GradleCommo模块！");
        String sub = StrUtil.sub("", 1, 2);
        System.out.println(entity1.getId1());



        return "我是GradleTwo模块！";
    }
}