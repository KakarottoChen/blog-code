package com.cc.go.service;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * <p></p>
 *
 * @author CC
 * @since 2023/10/26
 */
@Component
public class Test2Service {
    /**
     * 测试模块是否被启动
     */
    @PostConstruct
    public void get2(){
        System.out.println("Two模块启动。。。");
    }
}
