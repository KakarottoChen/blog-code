package com.cc.wsd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author cc
 */
@SpringBootApplication
public class WebServiceDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebServiceDemoApplication.class, args);
    }

}
