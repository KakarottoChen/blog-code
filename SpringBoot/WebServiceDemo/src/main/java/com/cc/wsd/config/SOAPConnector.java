//package com.cc.wsd.config;
//
//import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
//
///**
// * 对Web Service的所有请求的通用Web Client
// *
// * @author QuS
// * @date 2022/3/17 14:54
// */
//public class SOAPConnector extends WebServiceGatewaySupport {
//
//    public Object callWebService(String url, Object request) {
//
//        return getWebServiceTemplate().marshalSendAndReceive(url, request);
//
//    }
//
//}