//package com.cc.wsd.config;
//
//import org.springframework.boot.webservices.client.HttpWebServiceMessageSenderBuilder;
//import org.springframework.boot.webservices.client.WebServiceTemplateBuilder;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.oxm.jaxb.Jaxb2Marshaller;
//import org.springframework.ws.client.core.WebServiceTemplate;
//
//import java.time.Duration;
//
///**
// * @author CC
// * @since 2023/5/26 0026
// */
//@Configuration
//public class WebServiceTemplateConfig {
//
//
//    @Configuration
//    public class WebServiceConfig {
//
//        @Bean
//        public Jaxb2Marshaller marshaller() {
//            Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
//            marshaller.setContextPath("com.cc.wsd.entity");
//            return marshaller;
//        }
//
//        @Bean
//        public WebServiceTemplate webServiceTemplate(Jaxb2Marshaller marshaller) {
//            WebServiceTemplate template = new WebServiceTemplate();
//            template.setMarshaller(marshaller);
//            template.setUnmarshaller(marshaller);
//            template.setDefaultUri("http://ws.webxml.com.cn/WebServices/WeatherWS.asmx HTTP/1.1");
//            return template;
//        }
//    }
//
////    @Bean
////    public WebServiceTemplate webServiceTemplate(WebServiceTemplateBuilder builder) {
////        return builder.messageSenders(
////                new HttpWebServiceMessageSenderBuilder()
////                        //连接超时时间
////                .setConnectTimeout(Duration.ofMillis(5000))
////                        //读取超时时间
////                        .setReadTimeout(Duration.ofMillis(2000))
////                        .build()
////        ).build();
////    }
//
//}
