//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.2.11 生成的
// 请访问 <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2023.05.26 时间 02:35:32 PM CST 
//


package com.cc.wsd.xml;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.cc.wsd.xml package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CancelCommand_QNAME = new QName("http://dataservice.webService.service.mls.hac.com", "cancelCommand");
    private final static QName _CancelCommandResponse_QNAME = new QName("http://dataservice.webService.service.mls.hac.com", "cancelCommandResponse");
    private final static QName _GetCommandStatus_QNAME = new QName("http://dataservice.webService.service.mls.hac.com", "getCommandStatus");
    private final static QName _GetCommandStatusResponse_QNAME = new QName("http://dataservice.webService.service.mls.hac.com", "getCommandStatusResponse");
    private final static QName _GetDeviceCurrentData_QNAME = new QName("http://dataservice.webService.service.mls.hac.com", "getDeviceCurrentData");
    private final static QName _GetDeviceCurrentDataResponse_QNAME = new QName("http://dataservice.webService.service.mls.hac.com", "getDeviceCurrentDataResponse");
    private final static QName _GetDeviceFreezeData_QNAME = new QName("http://dataservice.webService.service.mls.hac.com", "getDeviceFreezeData");
    private final static QName _GetDeviceFreezeDataResponse_QNAME = new QName("http://dataservice.webService.service.mls.hac.com", "getDeviceFreezeDataResponse");
    private final static QName _GetDeviceImageData_QNAME = new QName("http://dataservice.webService.service.mls.hac.com", "getDeviceImageData");
    private final static QName _GetDeviceImageDataResponse_QNAME = new QName("http://dataservice.webService.service.mls.hac.com", "getDeviceImageDataResponse");
    private final static QName _GetDeviceInfo_QNAME = new QName("http://dataservice.webService.service.mls.hac.com", "getDeviceInfo");
    private final static QName _GetDeviceInfoResponse_QNAME = new QName("http://dataservice.webService.service.mls.hac.com", "getDeviceInfoResponse");
    private final static QName _ObtainDeviceCurrentData_QNAME = new QName("http://dataservice.webService.service.mls.hac.com", "obtainDeviceCurrentData");
    private final static QName _ObtainDeviceCurrentDataResponse_QNAME = new QName("http://dataservice.webService.service.mls.hac.com", "obtainDeviceCurrentDataResponse");
    private final static QName _RemoveDeviceInfo_QNAME = new QName("http://dataservice.webService.service.mls.hac.com", "removeDeviceInfo");
    private final static QName _RemoveDeviceInfoResponse_QNAME = new QName("http://dataservice.webService.service.mls.hac.com", "removeDeviceInfoResponse");
    private final static QName _SentCommand_QNAME = new QName("http://dataservice.webService.service.mls.hac.com", "sentCommand");
    private final static QName _SentCommandResponse_QNAME = new QName("http://dataservice.webService.service.mls.hac.com", "sentCommandResponse");
    private final static QName _SentHacCommand_QNAME = new QName("http://dataservice.webService.service.mls.hac.com", "sentHacCommand");
    private final static QName _SentHacCommandResponse_QNAME = new QName("http://dataservice.webService.service.mls.hac.com", "sentHacCommandResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.cc.wsd.xml
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CancelCommand }
     * 
     */
    public CancelCommand createCancelCommand() {
        return new CancelCommand();
    }

    /**
     * Create an instance of {@link CancelCommandResponse }
     * 
     */
    public CancelCommandResponse createCancelCommandResponse() {
        return new CancelCommandResponse();
    }

    /**
     * Create an instance of {@link GetCommandStatus }
     * 
     */
    public GetCommandStatus createGetCommandStatus() {
        return new GetCommandStatus();
    }

    /**
     * Create an instance of {@link GetCommandStatusResponse }
     * 
     */
    public GetCommandStatusResponse createGetCommandStatusResponse() {
        return new GetCommandStatusResponse();
    }

    /**
     * Create an instance of {@link GetDeviceCurrentData }
     * 
     */
    public GetDeviceCurrentData createGetDeviceCurrentData() {
        return new GetDeviceCurrentData();
    }

    /**
     * Create an instance of {@link GetDeviceCurrentDataResponse }
     * 
     */
    public GetDeviceCurrentDataResponse createGetDeviceCurrentDataResponse() {
        return new GetDeviceCurrentDataResponse();
    }

    /**
     * Create an instance of {@link GetDeviceFreezeData }
     * 
     */
    public GetDeviceFreezeData createGetDeviceFreezeData() {
        return new GetDeviceFreezeData();
    }

    /**
     * Create an instance of {@link GetDeviceFreezeDataResponse }
     * 
     */
    public GetDeviceFreezeDataResponse createGetDeviceFreezeDataResponse() {
        return new GetDeviceFreezeDataResponse();
    }

    /**
     * Create an instance of {@link GetDeviceImageData }
     * 
     */
    public GetDeviceImageData createGetDeviceImageData() {
        return new GetDeviceImageData();
    }

    /**
     * Create an instance of {@link GetDeviceImageDataResponse }
     * 
     */
    public GetDeviceImageDataResponse createGetDeviceImageDataResponse() {
        return new GetDeviceImageDataResponse();
    }

    /**
     * Create an instance of {@link GetDeviceInfo }
     * 
     */
    public GetDeviceInfo createGetDeviceInfo() {
        return new GetDeviceInfo();
    }

    /**
     * Create an instance of {@link GetDeviceInfoResponse }
     * 
     */
    public GetDeviceInfoResponse createGetDeviceInfoResponse() {
        return new GetDeviceInfoResponse();
    }

    /**
     * Create an instance of {@link ObtainDeviceCurrentData }
     * 
     */
    public ObtainDeviceCurrentData createObtainDeviceCurrentData() {
        return new ObtainDeviceCurrentData();
    }

    /**
     * Create an instance of {@link ObtainDeviceCurrentDataResponse }
     * 
     */
    public ObtainDeviceCurrentDataResponse createObtainDeviceCurrentDataResponse() {
        return new ObtainDeviceCurrentDataResponse();
    }

    /**
     * Create an instance of {@link RemoveDeviceInfo }
     * 
     */
    public RemoveDeviceInfo createRemoveDeviceInfo() {
        return new RemoveDeviceInfo();
    }

    /**
     * Create an instance of {@link RemoveDeviceInfoResponse }
     * 
     */
    public RemoveDeviceInfoResponse createRemoveDeviceInfoResponse() {
        return new RemoveDeviceInfoResponse();
    }

    /**
     * Create an instance of {@link SentCommand }
     * 
     */
    public SentCommand createSentCommand() {
        return new SentCommand();
    }

    /**
     * Create an instance of {@link SentCommandResponse }
     * 
     */
    public SentCommandResponse createSentCommandResponse() {
        return new SentCommandResponse();
    }

    /**
     * Create an instance of {@link SentHacCommand }
     * 
     */
    public SentHacCommand createSentHacCommand() {
        return new SentHacCommand();
    }

    /**
     * Create an instance of {@link SentHacCommandResponse }
     * 
     */
    public SentHacCommandResponse createSentHacCommandResponse() {
        return new SentHacCommandResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelCommand }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dataservice.webService.service.mls.hac.com", name = "cancelCommand")
    public JAXBElement<CancelCommand> createCancelCommand(CancelCommand value) {
        return new JAXBElement<CancelCommand>(_CancelCommand_QNAME, CancelCommand.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelCommandResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dataservice.webService.service.mls.hac.com", name = "cancelCommandResponse")
    public JAXBElement<CancelCommandResponse> createCancelCommandResponse(CancelCommandResponse value) {
        return new JAXBElement<CancelCommandResponse>(_CancelCommandResponse_QNAME, CancelCommandResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCommandStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dataservice.webService.service.mls.hac.com", name = "getCommandStatus")
    public JAXBElement<GetCommandStatus> createGetCommandStatus(GetCommandStatus value) {
        return new JAXBElement<GetCommandStatus>(_GetCommandStatus_QNAME, GetCommandStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCommandStatusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dataservice.webService.service.mls.hac.com", name = "getCommandStatusResponse")
    public JAXBElement<GetCommandStatusResponse> createGetCommandStatusResponse(GetCommandStatusResponse value) {
        return new JAXBElement<GetCommandStatusResponse>(_GetCommandStatusResponse_QNAME, GetCommandStatusResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDeviceCurrentData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dataservice.webService.service.mls.hac.com", name = "getDeviceCurrentData")
    public JAXBElement<GetDeviceCurrentData> createGetDeviceCurrentData(GetDeviceCurrentData value) {
        return new JAXBElement<GetDeviceCurrentData>(_GetDeviceCurrentData_QNAME, GetDeviceCurrentData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDeviceCurrentDataResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dataservice.webService.service.mls.hac.com", name = "getDeviceCurrentDataResponse")
    public JAXBElement<GetDeviceCurrentDataResponse> createGetDeviceCurrentDataResponse(GetDeviceCurrentDataResponse value) {
        return new JAXBElement<GetDeviceCurrentDataResponse>(_GetDeviceCurrentDataResponse_QNAME, GetDeviceCurrentDataResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDeviceFreezeData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dataservice.webService.service.mls.hac.com", name = "getDeviceFreezeData")
    public JAXBElement<GetDeviceFreezeData> createGetDeviceFreezeData(GetDeviceFreezeData value) {
        return new JAXBElement<GetDeviceFreezeData>(_GetDeviceFreezeData_QNAME, GetDeviceFreezeData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDeviceFreezeDataResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dataservice.webService.service.mls.hac.com", name = "getDeviceFreezeDataResponse")
    public JAXBElement<GetDeviceFreezeDataResponse> createGetDeviceFreezeDataResponse(GetDeviceFreezeDataResponse value) {
        return new JAXBElement<GetDeviceFreezeDataResponse>(_GetDeviceFreezeDataResponse_QNAME, GetDeviceFreezeDataResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDeviceImageData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dataservice.webService.service.mls.hac.com", name = "getDeviceImageData")
    public JAXBElement<GetDeviceImageData> createGetDeviceImageData(GetDeviceImageData value) {
        return new JAXBElement<GetDeviceImageData>(_GetDeviceImageData_QNAME, GetDeviceImageData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDeviceImageDataResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dataservice.webService.service.mls.hac.com", name = "getDeviceImageDataResponse")
    public JAXBElement<GetDeviceImageDataResponse> createGetDeviceImageDataResponse(GetDeviceImageDataResponse value) {
        return new JAXBElement<GetDeviceImageDataResponse>(_GetDeviceImageDataResponse_QNAME, GetDeviceImageDataResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDeviceInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dataservice.webService.service.mls.hac.com", name = "getDeviceInfo")
    public JAXBElement<GetDeviceInfo> createGetDeviceInfo(GetDeviceInfo value) {
        return new JAXBElement<GetDeviceInfo>(_GetDeviceInfo_QNAME, GetDeviceInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDeviceInfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dataservice.webService.service.mls.hac.com", name = "getDeviceInfoResponse")
    public JAXBElement<GetDeviceInfoResponse> createGetDeviceInfoResponse(GetDeviceInfoResponse value) {
        return new JAXBElement<GetDeviceInfoResponse>(_GetDeviceInfoResponse_QNAME, GetDeviceInfoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtainDeviceCurrentData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dataservice.webService.service.mls.hac.com", name = "obtainDeviceCurrentData")
    public JAXBElement<ObtainDeviceCurrentData> createObtainDeviceCurrentData(ObtainDeviceCurrentData value) {
        return new JAXBElement<ObtainDeviceCurrentData>(_ObtainDeviceCurrentData_QNAME, ObtainDeviceCurrentData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtainDeviceCurrentDataResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dataservice.webService.service.mls.hac.com", name = "obtainDeviceCurrentDataResponse")
    public JAXBElement<ObtainDeviceCurrentDataResponse> createObtainDeviceCurrentDataResponse(ObtainDeviceCurrentDataResponse value) {
        return new JAXBElement<ObtainDeviceCurrentDataResponse>(_ObtainDeviceCurrentDataResponse_QNAME, ObtainDeviceCurrentDataResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveDeviceInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dataservice.webService.service.mls.hac.com", name = "removeDeviceInfo")
    public JAXBElement<RemoveDeviceInfo> createRemoveDeviceInfo(RemoveDeviceInfo value) {
        return new JAXBElement<RemoveDeviceInfo>(_RemoveDeviceInfo_QNAME, RemoveDeviceInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveDeviceInfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dataservice.webService.service.mls.hac.com", name = "removeDeviceInfoResponse")
    public JAXBElement<RemoveDeviceInfoResponse> createRemoveDeviceInfoResponse(RemoveDeviceInfoResponse value) {
        return new JAXBElement<RemoveDeviceInfoResponse>(_RemoveDeviceInfoResponse_QNAME, RemoveDeviceInfoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SentCommand }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dataservice.webService.service.mls.hac.com", name = "sentCommand")
    public JAXBElement<SentCommand> createSentCommand(SentCommand value) {
        return new JAXBElement<SentCommand>(_SentCommand_QNAME, SentCommand.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SentCommandResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dataservice.webService.service.mls.hac.com", name = "sentCommandResponse")
    public JAXBElement<SentCommandResponse> createSentCommandResponse(SentCommandResponse value) {
        return new JAXBElement<SentCommandResponse>(_SentCommandResponse_QNAME, SentCommandResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SentHacCommand }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dataservice.webService.service.mls.hac.com", name = "sentHacCommand")
    public JAXBElement<SentHacCommand> createSentHacCommand(SentHacCommand value) {
        return new JAXBElement<SentHacCommand>(_SentHacCommand_QNAME, SentHacCommand.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SentHacCommandResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dataservice.webService.service.mls.hac.com", name = "sentHacCommandResponse")
    public JAXBElement<SentHacCommandResponse> createSentHacCommandResponse(SentHacCommandResponse value) {
        return new JAXBElement<SentHacCommandResponse>(_SentHacCommandResponse_QNAME, SentHacCommandResponse.class, null, value);
    }

}
