package com.cc.jxtd.web.controller;

import com.cc.jxtd.annotation.OptConverter;
import com.cc.jxtd.entity.UserCs;
import com.cc.jxtd.entity.UserCs2;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p></p>
 *
 * @author --
 * @since 2024/4/19
 */
@RestController
@RequestMapping("/userCs")
public class UserController {

//    @PostMapping
//    public UserCs get(@RequestBody UserCs req){
//        System.out.println("请求参数-id："   + req.getId());
//        System.out.println("请求参数-name：" + req.getName());
//        System.out.println("请求参数-DescSerialize：" + req.getDescSerialize());
//        System.out.println("请求参数-DescConverterNull：" + req.getDescConverterNull());
//        System.out.println("请求参数-DescConverter0：" + req.getDescConverter0());
//
//        //返回：序列化
//        return req;
//    }

    @OptConverter
    @PostMapping
    public UserCs2 get(@RequestBody UserCs2 req){
        System.out.println("请求参数-id："   + req.getId());
        System.out.println("请求参数-name：" + req.getName());
        System.out.println("请求参数-desc：" + req.getDesc());

        //返回：序列化
        return req;
    }

}

