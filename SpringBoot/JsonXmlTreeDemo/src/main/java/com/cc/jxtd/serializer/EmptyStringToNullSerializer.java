package com.cc.jxtd.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

/** 序列化：String的空转为null
 * @author --
 * @since 2024/4/18
 **/
public class EmptyStringToNullSerializer extends JsonSerializer<String> {

    /**
     * 序列化为null
     */
    @Override
    public void serialize(String value, JsonGenerator gen, SerializerProvider serializerProvider) throws IOException {
        if (value == null || value.trim().isEmpty()) {
            gen.writeNull();
        }else {
            gen.writeString(value);
        }
    }
}