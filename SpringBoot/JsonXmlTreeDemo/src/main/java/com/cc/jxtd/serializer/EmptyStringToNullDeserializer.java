package com.cc.jxtd.serializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;

/** 反序列化：空字符串转换为null
 * @author --
 * @since 2024/4/18
 **/
public class EmptyStringToNullDeserializer extends JsonDeserializer<String> {

    /**
     * 反序列化为null
     */
    @Override
    public String deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        String value = p.getValueAsString();
        if (value == null || value.trim().isEmpty()) {
            return null;
        }
        return value;
    }

}