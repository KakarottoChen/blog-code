package com.cc.mgc._main;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.converts.DmTypeConvert;
import com.baomidou.mybatisplus.generator.config.converts.MySqlTypeConvert;
import com.baomidou.mybatisplus.generator.config.converts.OracleTypeConvert;
import com.baomidou.mybatisplus.generator.config.converts.SqlServerTypeConvert;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

import java.util.*;

/** 代码生成
 * @since 2023/3/17 0017
 * @author CC
 **/
public class GenerateCode {

    /**
     * 需生成的表名
     */
    public final static String[] TABLES = {
            "multi_question"
    };
    /**
     * 一般为：web.controller。也可以为 rest
     */
    public final static String WEB_CONTROLLER = "web.controller";
    /**
     * 前缀。以上例子为：SYS_
     * 为空，生成实体类等会保留；
     * 输入前缀，生成实体类等，会删除前缀
     */
    public final static String[] PREFIX = {};

    /** 生成代码
     * @since 2023/3/20 0020
     * @author CC
     **/
    public static void main(String[] args) throws InterruptedException {
//        if(true) {return;}

        //用来获取Mybatis-Plus.properties文件的配置信息
        //修改配置文件名称，【[不要加后缀]】
        ResourceBundle rb = ResourceBundle.getBundle("mybatiesplus-config");
        AutoGenerator mpg = new AutoGenerator();
        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        //获取输出路劲
        gc.setOutputDir(rb.getString("OutputDir"));
        gc.setFileOverride(true);
        // 开启 activeRecord 模式
        gc.setActiveRecord(true);
        // XML 二级缓存
        gc.setEnableCache(false);
        // XML ResultMap
        gc.setBaseResultMap(true);
        // XML columList
        gc.setBaseColumnList(false);
        //获取作者
        gc.setAuthor(rb.getString("author"));
        //全局配置
        mpg.setGlobalConfig(gc);

        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        //mysql 数据库
        dsc.setDbType(DbType.MYSQL);
        dsc.setTypeConvert(new MySqlTypeConvert());
        //sqlserver 数据库
//        dsc.setDbType(DbType.SQL_SERVER);
//        dsc.setTypeConvert(new SqlServerTypeConvert());
        //达梦 数据库
//        dsc.setDbType(DbType.DM);
//        dsc.setTypeConvert(new DmTypeConvert());
        //oracle
//        dsc.setDbType(DbType.ORACLE);
//        dsc.setTypeConvert(new OracleTypeConvert());

        dsc.setDriverName(rb.getString("jdbc.driver"));
        dsc.setUsername(rb.getString("jdbc.user"));
        dsc.setPassword(rb.getString("jdbc.pwd"));
        dsc.setUrl(rb.getString("jdbc.url"));
        mpg.setDataSource(dsc);
        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        // 此处可以修改为您的【表前缀】
        strategy.setTablePrefix(PREFIX);
        // 表名生成策略：下划线转驼峰命名
        strategy.setNaming(NamingStrategy.underline_to_camel);
        // 添加需要生成的表  —— sqlServer的话 也 不需要使用：模式名.表明
        strategy.setInclude(TABLES);
        mpg.setStrategy(strategy);
        // 包配置 —— 生产的包的名字
        PackageConfig pc = new PackageConfig();
        pc.setParent(rb.getString("parent"));
        pc.setController(WEB_CONTROLLER);
        pc.setService("service");
        pc.setServiceImpl("service.impl");
        pc.setEntity("entity");
        pc.setMapper("mapper");
        mpg.setPackageInfo(pc);

        // 注入自定义配置，可以在 VM 中使用 cfg.abc 【可无】
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
                Map<String, Object> map = new HashMap<>();
                map.put("abc", this.getConfig().getGlobalConfig().getAuthor() + "-rb");
                this.setMap(map);
            }
        };

        String parent2 = rb.getString("parent2");
        List<FileOutConfig> focList = new ArrayList<>();
        // 调整 domain 生成目录演示 修改包名 —— 生产到 common 模块下去
        focList.add(new FileOutConfig("/templates/entity.java.vm") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                return rb.getString("OutputDirBase")+ parent2 + "/entity/"
                        + tableInfo.getEntityName() + ".java";
            }
        });

        // 调整 xml 生成目录演示  修改包名
        focList.add(new FileOutConfig("/templates/mapper.xml.vm") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                return rb.getString("OutputDirXml")+ parent2 + "/mapper/"
                        + tableInfo.getEntityName() + "Mapper.xml";
            }
        });

        // 增加 controller 的输出配置  修改包名  —— 生产到java下面
        String web_c = WEB_CONTROLLER.replace(".","/");
        focList.add(new FileOutConfig("/templates/controller.java.vm") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                return rb.getString("OutputDir")+ parent2 + "/"+ web_c +"/"
                        + tableInfo.getEntityName() + "Controller.java";
            }
        });

        // 增加 query 的输出配置  修改包名  —— 生产到 common 模块下去下面
        focList.add(new FileOutConfig("/templates/query.java.vm") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                return rb.getString("OutputDirBase")+ parent2 +"/query/"
                        + tableInfo.getEntityName() + "Query.java";
            }
        });

        cfg.setFileOutConfigList(focList);
        mpg.setCfg(cfg);

        // 使用mybatis-plus的模板
        // 可以 copy 源码 mybatis-plus/src/main/resources/templates 下面内容修改，
        // 放置自己项目的 src/main/resources/templates 目录下, 默认名称一下可以不配置，也可以自定义模板名称
        TemplateConfig tc = new TemplateConfig();
        tc.setService("/templates/service.java.vm");
        tc.setServiceImpl("/templates/serviceImpl.java.vm");
        tc.setMapper("/templates/mapper.java.vm");
        //使用 我们自己的模板
        tc.setEntity(null);
        tc.setController(null);
        tc.setXml(null);
        // 如上任何一个模块如果设置 空 OR Null 将不生成该模块。
        mpg.setTemplate(tc);

        // 执行生成
        mpg.execute();
    }

}