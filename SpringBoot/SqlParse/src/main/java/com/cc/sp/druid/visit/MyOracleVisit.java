package com.cc.sp.druid.visit;

import com.alibaba.druid.sql.ast.statement.SQLExprTableSource;
import com.alibaba.druid.sql.ast.statement.SQLSelectQueryBlock;
import com.alibaba.druid.sql.ast.statement.SQLTableSource;
import com.alibaba.druid.sql.dialect.oracle.visitor.OracleASTVisitorAdapter;

import java.util.HashSet;
import java.util.Set;

/** Oracle的sql树
 * @since 2023/5/4 0004
 * @author CC
 **/
public class MyOracleVisit extends OracleASTVisitorAdapter {

    /**
     *  visit tables
     */
    Set<String> tables = new HashSet<>();

    /**
     * sql的别名
     */
    String alias = "";

    @Override
    public boolean visit(SQLExprTableSource x) {
        String tableName = x.getTableName();
        tables.add(tableName);
        return super.visit(x);
    }

    /** 获取查询语句的别名
     * @param x x
     * @return boolean
     * @since 2023/4/19 0019
     * @author CC
     **/
    @Override
    public boolean visit(SQLSelectQueryBlock x) {
        SQLTableSource from = x.getFrom();
        alias = from.getAlias();
        return super.visit(x);
    }


    /** oracle获取连表的表名、连表的别名；获取单表的别名
     * @return boolean
     * @since 2023/4/19 0019
     * @author CC
     **/
//    @Override
//    public boolean visit(OracleSelectTableReference x) {
//    public boolean visit(OracleSelectQueryBlock x) {
//        SQLTableSource from = x.getFrom();
//        if (StringUtils.isBlank(alias)){
//            alias = from.getAlias();
//        }
//        return super.visit(x);
//    }

    /** oracle获取最终别名
     * @since 2023/4/19 0019
     * @author CC
     **/
//    @Override
//    public void endVisit(OracleSelectSubqueryTableSource x) {
//        alias = x.getAlias();
//        super.endVisit(x);
//    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public Set<String> getTables() {
        return tables;
    }

    public void setTables(Set<String> tables) {
        this.tables = tables;
    }

}
