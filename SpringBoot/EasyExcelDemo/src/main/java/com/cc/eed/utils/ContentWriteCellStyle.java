package com.cc.eed.utils;

import com.alibaba.excel.write.metadata.style.WriteCellStyle;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.springframework.stereotype.Component;

/**
 * <p>内容样式 策略 —— 单例模式</p>
 *
 * @author CC
 * @since 2023/10/17
 */
public class ContentWriteCellStyle extends WriteCellStyle {

    private ContentWriteCellStyle() {
    }

    /**
     * 单例模式 - 静态内部类模式
     */
    private static final class InstanceHolder {
        static final ContentWriteCellStyle INSTANCE = new ContentWriteCellStyle();
    }

    /**
     * 自定义内容样式
     */
    public static ContentWriteCellStyle getInstance(){
        ContentWriteCellStyle writeCellStyle = InstanceHolder.INSTANCE;
        // 水平对齐方式：居中
        writeCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        // 垂直对齐方式：居中
        writeCellStyle.setHorizontalAlignment(HorizontalAlignment.CENTER);
        // 设置边框线
        writeCellStyle.setBorderBottom(BorderStyle.THIN);
        writeCellStyle.setBorderTop(BorderStyle.THIN);
        writeCellStyle.setBorderLeft(BorderStyle.THIN);
        writeCellStyle.setBorderRight(BorderStyle.THIN);
        return writeCellStyle;
    }


}
