package com.cc.eed.utils;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.write.metadata.style.WriteCellStyle;
import com.alibaba.excel.write.style.HorizontalCellStyleStrategy;
import com.alibaba.excel.write.style.column.SimpleColumnWidthStyleStrategy;
import com.alibaba.excel.write.style.row.SimpleRowHeightStyleStrategy;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.net.URLEncoder;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

/**
 * <p></p>
 *
 * @author CC
 * @since 2023/10/16
 */
public class EasyExcelUtil {

    /** 导出excel
     * @param datas 数据
     * @param fileName excel的文件名（自动.xlsx结尾）
     * @param clazz 映射
     * @param response 响应
     * @since 2022/10/29 0029
     * @author CC
     **/
    public static void excelExport(List<?> datas,
                                   String fileName,
                                   Class<?> clazz,
                                   HttpServletResponse response) {

        try (ServletOutputStream out = response.getOutputStream()) {
            //重构文件名
            String suffix = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HHmmssSSS"));
            fileName = URLEncoder.encode(fileName.concat("_").concat(suffix).concat(".xlsx"), "UTF-8");

            response.setCharacterEncoding("utf-8");
            response.setContentType("application/vnd.ms-excel");
            //Access-Control-Expose-Headers ：解决跨域不显示在header里面的问题
//            response.setHeader("Access-Control-Expose-Headers","Content-disposition,excelName");
            response.setHeader("Access-Control-Expose-Headers","Content-disposition");
            response.setHeader("Content-disposition", String.format("attachment; filename=%s", fileName));

            //头策略使用默认 设置字体大小
            WriteCellStyle headWriteCellStyle = new WriteCellStyle();
            //导出Excel
            EasyExcel.write(out,clazz).sheet(1)
                    // 注册策略：简单的列宽策略，列宽
                    .registerWriteHandler(new SimpleColumnWidthStyleStrategy(15))
                    // 注册策略：简单的行高策略：头行高，内容行高
                    .registerWriteHandler(new SimpleRowHeightStyleStrategy((short)25, (short)25))
                    // 注册策略：表头、内容
                    .registerWriteHandler(new HorizontalCellStyleStrategy(
                            headWriteCellStyle,ContentWriteCellStyle.getInstance()
                    ))
                    .doWrite(datas);
        }catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * 获取修改ExcelProperty的value值的class，用于导出
     * @param t 类对象
     * @param keys key为修改的字段和value为它对应表头值
     * @return
     */
    public static <T> Class<T> getClassNew(T t, Map<String, String> keys){
        if(t == null){
            return null;
        }
        try{
            for(String key: keys.keySet()) {
                Field value = t.getClass().getDeclaredField(key);
                value.setAccessible(true);
                ExcelProperty property = value.getAnnotation(ExcelProperty.class);
                InvocationHandler invocationHandler = Proxy.getInvocationHandler(property);
                Field memberValues = invocationHandler.getClass().getDeclaredField("memberValues");
                memberValues.setAccessible(true);
                Map<String, Object> values = (Map<String, Object>) memberValues.get(invocationHandler);
                values.put("value", new String[]{keys.get(key)});
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return (Class<T>) t.getClass();
    }

}
