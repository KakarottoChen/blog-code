package com.cc.eed.service.impl;

import com.cc.eed.build.UserCsBuild;
import com.cc.eed.entity.UserCs;
import com.cc.eed.service.IUserCsService;
import com.cc.eed.utils.EasyExcelUtil;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p></p>
 *
 * @author CC
 * @since 2023/10/16
 */
@Service
@Component
public class UserCsServiceImpl implements IUserCsService {

    @Override
    public void get(HttpServletResponse response){
        List<UserCs> list = UserCsBuild.getList();

        //自定义表头的内容
        // key为需要修改的字段名，value为该字段名对应的ExcelProperty的值
        Map<String, String> keys = new HashMap<>();
        keys.put("id", "主键");
        keys.put("name", "合并");
        keys.put("age", "合并2");
        keys.put("sex", "合并3");
//        keys.put("orderDate", "订单日期");

        Class<Class<UserCs>> classNew = EasyExcelUtil.getClassNew(UserCs.class, keys);
        EasyExcelUtil.excelExport(list, "文件名", classNew, response);


    }


}
