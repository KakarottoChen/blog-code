package com.cc.eed.bean;

import org.springframework.stereotype.Component;

/**
 * <p></p>
 *
 * @author CC
 * @since 2023/10/12
 */
@Component
public class Bean1 {

    public String getBean1() {
        return "Bean1";
    }

}
