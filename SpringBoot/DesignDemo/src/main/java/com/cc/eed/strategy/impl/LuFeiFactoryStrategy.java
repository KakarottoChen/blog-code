package com.cc.eed.strategy.impl;

import com.cc.eed.enums.PirateEnum;
import com.cc.eed.strategy.IFactoryStrategy;
import org.springframework.stereotype.Component;

/**
 * <p>路飞</p>
 *
 * @author CC
 * @since 2023/10/13
 */
@Component(PirateEnum.LF_BEAN_NAME)
public class LuFeiFactoryStrategy implements IFactoryStrategy {
    /**
     * 吃饭
     */
    @Override
    public String eating() {
        return "路飞，吃饭！";
    }

    /**
     * 玩
     */
    @Override
    public String play() {
        return "路飞，玩！";
    }
}
