package com.cc.eed.strategy.impl;

import com.cc.eed.enums.PirateEnum;
import com.cc.eed.strategy.ISpringBeanStrategy;
import org.springframework.stereotype.Component;

/**
 * <p>小美</p>
 *
 * @author CC
 * @since 2023/10/13
 */
@Component
public class MeiSpringBeanImpl implements ISpringBeanStrategy {

    /**
     * 吃饭
     */
    @Override
    public String eating() {
        return "小美，吃饭！";
    }

    /**
     * 玩
     */
    @Override
    public String play() {
        return "小美，玩！";
    }

}
