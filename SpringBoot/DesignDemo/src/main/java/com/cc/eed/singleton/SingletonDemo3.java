package com.cc.eed.singleton;

/**
 * <p>单例模式 - 静态内部类模式</p>
 *
 * @author CC
 * @since 2023/10/17
 */
public class SingletonDemo3 {

    private SingletonDemo3() {
    }

    /**
     * 静态内部类 生产单例
     */
    private static final class InstanceHolder {
        static final SingletonDemo3 INSTANCE = new SingletonDemo3();
    }

    public static SingletonDemo3 getInstance(){
        return InstanceHolder.INSTANCE;
    }

}
