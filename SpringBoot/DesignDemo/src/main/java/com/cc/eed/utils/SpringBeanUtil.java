package com.cc.eed.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

/**
 * <p>获取spring的bean的工具类</p>
 *
 * @author CC
 * @since 2023/10/12
 */
@Component
public class SpringBeanUtil implements ApplicationContextAware {

    private static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        SpringBeanUtil.applicationContext = applicationContext;
    }

    /** <p>根据类的clazz获取<p>
     * @param clazz clazz
     * @return {@link T}
     * @since 2023/10/12
     * @author CC
     **/
    public static <T> T getBean(Class<T> clazz) {
        T t = applicationContext != null ? applicationContext.getBean(clazz) : null;
        Assert.notNull(t, "当前Bean为空，请重新选择！");
        return t;
    }

    /** <p>根据Bean名字获取<p>
     * @param clazzName bean名字
     * @return {@link Object}
     * @since 2023/10/12
     * @author CC
     **/
    public static Object getBean(String clazzName) {
        return applicationContext.getBean(clazzName);
    }

}
