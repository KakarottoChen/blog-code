package com.cc.eed.enums;

import org.springframework.util.Assert;

import java.util.Arrays;

/**
 * <p></p>
 *
 * @author CC
 * @since 2023/10/13
 */
public enum PirateEnum {

    MG(11, "明哥", PirateEnum.MG_BEAN_NAME),

    LF(22, "路飞", PirateEnum.LF_BEAN_NAME)

    ;

    public Integer type;

    public String name;

    public String beanName;

    /**
     * 自定义的beanName
     */
    public static final String MG_BEAN_NAME = "mingGg_11";
    public static final String LF_BEAN_NAME = "luFei_22";

    /** <p>根据类型获取beanName<p>
     * @param type type
     * @return {@link String}
     * @since 2023/10/13
     * @author CC
     **/
    public static String getBeanName(Integer type) {
        PirateEnum pirateEnum = Arrays.stream(values())
                .filter(p -> p.getType().equals(type))
                .findAny().orElse(null);
        Assert.notNull(pirateEnum, "暂不支持的策略模式！");
        return pirateEnum.getBeanName();
    }

    PirateEnum(Integer type, String name, String beanName) {
        this.type = type;
        this.name = name;
        this.beanName = beanName;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBeanName() {
        return beanName;
    }

    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }
}
