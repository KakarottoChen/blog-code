package com.cc.eed.strategy.impl;

import com.cc.eed.enums.PirateEnum;
import com.cc.eed.strategy.IFactoryStrategy;
import org.springframework.stereotype.Component;

/**
 * <p>明哥</p>
 *
 * @author CC
 * @since 2023/10/13
 */
@Component(PirateEnum.MG_BEAN_NAME)
public class MingGgFactoryStrategy implements IFactoryStrategy {
    /**
     * 吃饭
     */
    @Override
    public String eating() {
        return "明哥，吃饭！";
    }

    /**
     * 玩
     */
    @Override
    public String play() {
        return "明哥，玩！";
    }
}
