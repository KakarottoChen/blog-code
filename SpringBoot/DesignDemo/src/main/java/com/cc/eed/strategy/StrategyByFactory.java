package com.cc.eed.strategy;

import com.cc.eed.enums.PirateEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * <p>简单工厂</p>
 * <li>可以生产多个策略</li>
 *
 * @author CC
 * @since 2023/10/13
 */
@Component
public class StrategyByFactory {

    /**
     * 1、批量注入实现了 IFactoryStrategy 的Bean。
     * 2、用bean的数量当做map的大小
     */
    @Resource
    private final Map<String, IFactoryStrategy> BUSINESS_FACTORY = new ConcurrentHashMap<>(PirateEnum.values().length);

    //生成的策略...
    /** <p>根据类获取不同的Bean<p>
     * @param type type
     * @return {@link IFactoryStrategy}
     * @since 2023/10/13
     * @author CC
     **/
    public IFactoryStrategy getBusinessMap(Integer type){
        Assert.notNull(type, "类型不能为空！");
        String beanName = PirateEnum.getBeanName(type);
        return BUSINESS_FACTORY.get(beanName);
    }

    //生成的其他策略...

}
