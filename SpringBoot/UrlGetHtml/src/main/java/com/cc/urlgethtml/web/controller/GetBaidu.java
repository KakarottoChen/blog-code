package com.cc.urlgethtml.web.controller;

import com.cc.urlgethtml.utils.JsoupUtils;
import com.cc.urlgethtml.utils.PhantomPath2;
import com.cc.urlgethtml.utils.PhantomTools;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.w3c.dom.Document;
import org.xhtmlrenderer.swing.Java2DRenderer;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Map;

/**
 * <p></p>
 *
 * @author CC
 * @since 2023/10/31
 */
@RestController
@RequestMapping("/urlGetHtml")
public class GetBaidu {


    @Resource
    private PhantomPath2 phantomPath2;

    @GetMapping("/getCon")
    public String getCon(){
        Map<String, String> binPathWin = phantomPath2.getBinPath();
        String string = binPathWin.get("1");
        return string;
    }

    //学习：https://blog.csdn.net/listeningsea/article/details/132342977
    @Resource
    private PhantomTools phantomTools;

    /** <p>通过Java2DRenderer将html转为图片</p>
     * <ol>
     *     <li>通过jsoup根据url获取静态html</li>
     *     <li>使用Java2DRenderer将静态html转为图片，浏览器下载</li>
     * </ol>
     *
     * <li>优点：不用第三方插件。如果是标准的html页面，可以使用该方式</li>
     * <li>缺点：复杂的html转不了，直接会报错</li>
     */
    @GetMapping("/getJava2DRenderer")
    public String get1(HttpServletResponse response){

        //url
        String url = "http://www.baidu.com";
        //一、通过jsoup获取url的html（获取的html不完整，而且是渲染前的）
//        String html = JsoupUtils.getHtmlByJsoup(url);

        //二、模拟从url获取的：非常简单的html。测了从富文本中获取的html有可能都识别不了。恶心。
        String html = "<html><body><h1>Hello, World!</h1></body></html>";
        String fileName = "文件名.png";

        //转图片，有问题——复杂的html转不了
        Document document;
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            document = builder.parse(new ByteArrayInputStream(html.getBytes()));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        Java2DRenderer renderer = new Java2DRenderer(document, 480, 640);
        BufferedImage img = renderer.getImage();

        //方式一：浏览器下载
        try {
            response.setCharacterEncoding(StandardCharsets.UTF_8.name());
            response.setContentType("image/png;charset=".concat(StandardCharsets.UTF_8.name()));
            response.setHeader(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS,HttpHeaders.CONTENT_DISPOSITION);
            response.setHeader(HttpHeaders.CONTENT_DISPOSITION,
                    "attachment; filename=".concat(
                            URLEncoder.encode(fileName, StandardCharsets.UTF_8.name())
                    ));
            ServletOutputStream out = response.getOutputStream();

            //把 BufferedImage 缓冲图像流 写到 输出流 out 中
            ImageIO.write(img, "png", out);
            out.close();
        }catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }

        //方式二：写入本地
        /*FSImageWriter imageWriter = new FSImageWriter();
        try {
            imageWriter.write(img, "D:\\ABC.jpg");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }*/

        return html;
    }

    /** <p>PhantomJs截图<p>
     * <p>截图到本地，未下载到浏览器<p>
     * @return {@link String}
     * @since 2023/11/6
     * @author CC
     **/
    @GetMapping("/getPhantomJs")
    public void getPhantomJs(){
        String url = "https://www.jd.com/";
        try {
            phantomTools.printUrlScreen2jpg(url, "第一个图片.png");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


}
