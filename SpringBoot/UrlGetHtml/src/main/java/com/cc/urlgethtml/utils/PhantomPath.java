package com.cc.urlgethtml.utils;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * <p>根据不同系统获取不同路径</p>
 *
 * @author CC
 * @since 2023/11/3
 */
@Data
@Component
public class PhantomPath {

    @Value("${phantomjs.binPath.windows}")
    private String binPathWin;
    @Value("${phantomjs.jsPath.windows}")
    private String jsPathWin;
    @Value("${phantomjs.binPath.linux}")
    private String binPathLinux;
    @Value("${phantomjs.jsPath.linux}")
    private String jsPathLinux;
    @Value("${phantomjs.imagePath.windows}")
    private String imagePathWin;
    @Value("${phantomjs.imagePath.linux}")
    private String imagePathLinux;



    //获取当前系统是否是Windows系统（不是就是服务器，服务器默认Linux系统（centos））
    private static final Boolean IS_WINDOWS = System.getProperty("os.name").toLowerCase().contains("windows");

    public String getImagePath() {
        return IS_WINDOWS ? imagePathWin : imagePathLinux;
    }

    public String getBinPath() {
        return IS_WINDOWS ? binPathWin : binPathLinux;
    }

    public String getJsPath() {
        return IS_WINDOWS ? jsPathWin : jsPathLinux;
    }

    public boolean getIsWindows() {
        return IS_WINDOWS;
    }



}
