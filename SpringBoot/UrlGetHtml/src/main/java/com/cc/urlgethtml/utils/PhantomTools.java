package com.cc.urlgethtml.utils;

import cn.hutool.core.util.StrUtil;
import com.sun.jna.Platform;
import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.Kernel32;
import com.sun.jna.platform.win32.WinNT;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import javax.annotation.Resource;
import java.io.*;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/** <p>根据网页地址转换成图片</p>
 *  <p>条件：需要插件及js脚本</p>
 *  <p>缺陷：部分网址，截图样式部分还是有问题</p>
 * @since 2023/11/6
 * @author CC
 **/
@Data
@Slf4j
@Component
public class PhantomTools {

    @Resource
    private PhantomPath phantomPath;

    // token
    private static String cookie = "token=";

    /**
     * @param url 地址
     * @param imageName 图片全名
     * @since 2023/11/3
     **/
    public void printUrlScreen2jpg(String url, String imageName) throws IOException{
        //执行命令，生成图片
        String[] cmd = this.getCmd(url, imageName);
        Process process;
        if (phantomPath.getIsWindows()) {
            //一个参数：直接执行命令（）
            process = Runtime.getRuntime().exec(cmd);
        }else {
            String linuxPath = getBasePath().concat(phantomPath.getBinPathLinux());
            log.info("Linux执行目录：{}", linuxPath);
            process = Runtime.getRuntime().exec(cmd, new String[]{}, new File(linuxPath));
        }

        //读取执行日志
        InputStream inputStream = process.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        String msg;
        while ((msg = reader.readLine()) != null) {
            log.info("PhantomJs导出图片日志：{}", msg);
        }
        log.info("PhantomJs导出图片：{}", imageName);
        //关闭流
        close(process, reader);
    }

    // 生成cmd命令：可以是直接的字符串，也可以是string数组
    public String[] getCmd(String url, String imageName) throws FileNotFoundException {
        String basePath = getBasePath();

        List<String> list = new ArrayList<>();
        //Windows命令
        if (phantomPath.getIsWindows()) {
            list.add(basePath.concat(phantomPath.getBinPath()));
            list.add(basePath.concat(phantomPath.getJsPath()));
            list.add(url);
            list.add(String.format("%s/%s", phantomPath.getImagePath(), imageName));
        }

        //Linux命令
        else {
            list.add("./phantomjs");
            list.add(phantomPath.getJsPath());
            list.add(url);
            list.add(String.format("%s/%s", phantomPath.getImagePath(), imageName));
        }

        log.info("PhantomJs导出图片的cmd：{}", list);
        return list.toArray(new String[0]);
    }

    //获取项目跟目录的：plugins目录
    private static String getBasePath() throws FileNotFoundException {
        String basePath = ResourceUtils.getURL("plugins").getPath();
        return StrUtil.removePrefix(basePath, "/");
    }

    //关闭命令
    public static void close(Process process, BufferedReader bufferedReader) throws IOException {
        if (bufferedReader != null) {
            bufferedReader.close();
        }
        if (process != null) {
            process.destroy();
        }
    }

}
