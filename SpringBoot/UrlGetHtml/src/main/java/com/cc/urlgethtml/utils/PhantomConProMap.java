package com.cc.urlgethtml.utils;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * <p></p>
 *
 * @author CC
 * @since 2023/11/7
 */
@Data
@Component
//@ConfigurationProperties(prefix = "phantomjs")
public class PhantomConProMap {

    private Map<String, String> binPath;

    private Map<String, String> jsPath;

    private Map<String, String> imagePath;

}
