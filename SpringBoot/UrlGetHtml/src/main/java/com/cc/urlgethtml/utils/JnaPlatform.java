package com.cc.urlgethtml.utils;

import com.sun.jna.Platform;
import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.Kernel32;
import com.sun.jna.platform.win32.WinNT;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.util.Objects;
import java.util.Optional;

/**
 * <p>JnaPlatform：获取执行命令的pid</p>
 *
 * @author CC
 * @since 2023/11/6
 */
public class JnaPlatform {

    public static void main(String[] args) {
        Process process = null;
        try {
            //执行命令
            if (Platform.isWindows()) {
                process = Runtime.getRuntime().exec("java -version", new String[]{},
                        new File("D:\\"));
            }else {
                process = Runtime.getRuntime().exec("ls -l");
            }

            //获取日志
            InputStream inputStream = process.getErrorStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String msg;
            while ((msg = reader.readLine()) != null) {
                System.out.println(msg);
            }
            //获取pid
            Optional<Integer> pid = getPid(process);
            System.out.println("pid----" + pid.orElse(-1));

        }catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }finally {
            if (Objects.nonNull(process)) {
                process.destroy();
            }
        }
    }

    /** <p>获取pid<p>
     * @param process process
     * @return {@link Optional<Integer>}
     * @since 2023/11/6
     * @author CC
     **/
    public static Optional<Integer> getPid(Process process) {
        if (Platform.isLinux()) {
            /* Linux platform */
            try {
                Field pidField = process.getClass().getDeclaredField("pid");
                pidField.setAccessible(true);
                return Optional.of((Integer) pidField.get(process));
            } catch (NoSuchFieldException | IllegalAccessException e) {
                return Optional.empty();
            }
        } else if (Platform.isWindows()) {
            /* Windows platform */
            try {
                Field handleField = process.getClass().getDeclaredField("handle");
                handleField.setAccessible(true);
                long handl = (Long) handleField.get(process);
                Kernel32 kernel = Kernel32.INSTANCE;
                WinNT.HANDLE hand = new WinNT.HANDLE();
                hand.setPointer(Pointer.createConstant(handl));
                int pid = kernel.GetProcessId(hand);
                return Optional.of(pid);
            } catch (NoSuchFieldException | IllegalAccessException e) {
                return Optional.empty();
            }
        }
        return Optional.empty();
    }

    /** <p>根据pid停止进程-待实现<p>
     * @since 2023/11/6
     * @author CC
     **/
    public boolean kill(String pId){
        return true;
    }


}
