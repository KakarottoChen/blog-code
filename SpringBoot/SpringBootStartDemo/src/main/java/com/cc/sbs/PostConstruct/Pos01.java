package com.cc.sbs.PostConstruct;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/** @Order没用
 * @author CC
 * @since 2023/5/17 0017
 */
@Component
@Order(6)
public class Pos01 {

    @PostConstruct
    public void customizeName(){
        System.out.println("Pos01_执行了……@Order(6)");
    }

}
