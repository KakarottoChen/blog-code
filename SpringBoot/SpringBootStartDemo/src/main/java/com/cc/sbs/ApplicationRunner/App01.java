package com.cc.sbs.ApplicationRunner;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author CC
 * @since 2023/5/17 0017
 */
@Component
@Order(3)
public class App01 implements ApplicationRunner {

    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println("App01_执行了……@Order(3)");
    }

}


