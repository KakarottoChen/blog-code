package com.cc.sbs.InitializingBean;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/** @Order没用
 * @author CC
 * @since 2023/5/17 0017
 */
@Component
@Order(7)
public class Ini01 implements InitializingBean {

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("Ini01_执行了……@Order(7)");
    }

}
