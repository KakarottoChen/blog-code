package com.cc.testproject.websocket.web.controller;

import com.cc.testproject.websocket.utils.WebSocketUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;

/** 聊天跳转
 * @Description
 * @Author CC
 * @Date 2021/11/19
 * @Version 1.0
 */
@Controller
//说明创建WebSocket的endpoint
@ServerEndpoint("/chat/{username}")
public class ChatController {

    /** 响应聊天页面
     * @Description
     * @Author CC
     * @Date 2021/11/19
     * @Param []
     * @return java.lang.String
     **/
    @GetMapping("/chatPage")
    public String chatPage(){
        return "webSocket/chat";
    }

//WebSocket有4个事件，我们对每个事件做监听，使用对应的注解即可实现监听
    /**
     * onopen 在连接创建(用户进入聊天室)时触发
     * @param username 用户名
     * @param session sessionId
     */
    @OnOpen
    public void openSession(@PathParam("username") String username, Session session){
        //存储用户
        WebSocketUtil.USERS_ONLINE.put(username, session);
        //向所有在线用户发送用户上线通知消息
        String message = "["+username+"]进入聊天室";
        System.out.println(message);
        WebSocketUtil.sendMessageToAllOnlineUser(message);
    }

    /**
     * onclose 在连接断开(用户离开聊天室)时触发
     * @param username 用户名
     * @param session sessionId
     */
    @OnClose
    public void closeSession(@PathParam("username") String username, Session session){
        //删除用户
        WebSocketUtil.USERS_ONLINE.remove(username);
        //向所有在线用户发送用户下线通知消息
        String message = "["+username+"]离开了聊天室";
        System.out.println(message);
        WebSocketUtil.sendMessageToAllOnlineUser(message);
        //下线后关闭session
        try {
            session.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * onmessage 在接收到消息时触发
     * @param username
     * @param message
     */
    @OnMessage
    public void onMessage(@PathParam("username") String username, String message){
        //向聊天室中的人发送消息
        message = "["+username+"]：" + message;
        System.out.println(message);
        WebSocketUtil.sendMessageToAllOnlineUser(message);
    }

    /**
     * orerror 在连接发生异常时触发
     * @param session
     * @param throwable
     */
    @OnError
    public void sessionError(Session session, Throwable throwable){
        try {
            session.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("WebSocket连接发生异常，message:"+throwable.getMessage());
    }
}
