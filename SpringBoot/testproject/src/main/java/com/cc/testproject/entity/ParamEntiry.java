package com.cc.testproject.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description
 * @Author ChenCheng
 * @Date 2021/8/2 11:35
 * @Version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ParamEntiry {

    private String param1;

    private String param2;
}
