package com.cc.testproject.threadPool.controller;

import cn.hutool.core.util.StrUtil;
import com.cc.testproject.threadPool.async.AsyncService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Description
 * @Author CC
 * @Date 2022/1/18
 * @Version 1.0
 */
@RequestMapping("async")
@RestController
public class AsyncController {

    @Resource
    private AsyncService asyncService;

    /**调用异步
     */
    @GetMapping
    public Object openAsync(){
        long start = System.currentTimeMillis();
        //正常方法执行……

        //开启异步：需要分片处理，才会开启不同的线程获取任务
        //开启异步1，开启两次
        for (int i = 0; i < 2; i++) {
            asyncService.async01();
            System.out.println("主线程开启01。第几次：" + i);
        }
        //开启异步2，开启三次
        for (int i = 0; i < 3; i++) {
            asyncService.async02();
            System.out.println("主线程开启02。第几次：" + i);
        }
        System.out.println(StrUtil.format("总线程:{}执行完，耗时：{}ms",
                Thread.currentThread().getName(),System.currentTimeMillis() - start));
        return StrUtil.format("开启完成，耗时：{}ms",System.currentTimeMillis() - start);
    }

}
