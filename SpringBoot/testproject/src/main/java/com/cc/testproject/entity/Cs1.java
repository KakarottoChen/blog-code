package com.cc.testproject.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Date;

@Data
@TableName("t_cs1")
@NoArgsConstructor
@AllArgsConstructor
public class Cs1 {

    @TableId
    private Long id;
    @TableField("name")
    private String name;

    @TableField("date")
    private Date date;

    @TableField("local_date_time")
    private LocalDateTime localDateTime;
}