/*
package com.cc.testproject.activiti7.securityConfigUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

*/
/** 把用户信息存放在内存中（虚拟用户信息）
 * @Description
 * @Author CC
 * @Date 2021/12/20
 **//*

@Configuration
public class ActivitiSecurityCon {

    private Logger logger = LoggerFactory.getLogger(ActivitiSecurityCon.class);

    */
/** 添加Security的用户
     * @Description
     * @Author CC
     * @Date 2021/12/20
     * @Param []
     * @return org.springframework.security.core.userdetails.UserDetailsService
     **//*

     @Bean
     public UserDetailsService myUserDetailsService() {
         InMemoryUserDetailsManager inMemoryUserDetailsManager = new InMemoryUserDetailsManager();
         //这里添加用户，后面处理流程时用到的任务负责人，需要添加在这里
         String[][] usersGroupsAndRoles = {
                 //用户名    密码            角色                     组
                 {"zhangsan", "111111", "ROLE_ACTIVITI_USER", "GROUP_PTTeam"},
                 {"jishu", "111111", "ROLE_ACTIVITI_USER", "GROUP_jishuTeam"},
                 {"renshi", "111111", "ROLE_ACTIVITI_USER", "GROUP_renshiTeam"},
                 {"xiangmu", "111111", "ROLE_ACTIVITI_USER", "GROUP_xiangmuTeam"},
                 {"xiaoshou", "111111", "ROLE_ACTIVITI_USER", "GROUP_xiaoshouTeam"},
                 {"zongjingli", "111111", "ROLE_ACTIVITI_USER", "GROUP_zongjingliTeam"},
                 {"system", "111111", "ROLE_ACTIVITI_USER"},
                 {"admin", "111111", "ROLE_ACTIVITI_ADMIN"},
         };

         for (String[] user : usersGroupsAndRoles) {
             List<String> authoritiesStrings = Arrays.asList(Arrays.copyOfRange(user, 2, user.length));
             logger.info("Security中注册新用户: {}。角色信息：[{}]",user[0],authoritiesStrings);
             inMemoryUserDetailsManager.createUser(new User(user[0], passwordEncoder().encode(user[1]),
                     authoritiesStrings.stream().map(s -> new SimpleGrantedAuthority(s)).collect(Collectors.toList())));
         }

         return inMemoryUserDetailsManager;
     }
     @Bean
     public PasswordEncoder passwordEncoder() {
         return new BCryptPasswordEncoder();
     }
}*/
