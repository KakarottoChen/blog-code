package com.cc.testproject.web.controller.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cc.testproject.entity.Cs1;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author CC
 * @since 2022/3/16
 */
@Mapper
public interface Cs1Mapper extends BaseMapper<Cs1> {


}
