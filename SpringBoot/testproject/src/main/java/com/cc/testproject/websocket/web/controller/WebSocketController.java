package com.cc.testproject.websocket.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Description
 * @Author CC
 * @Date 2021/11/19
 **/
@Controller
public class WebSocketController {

    /**
     * 跳转页面到
     * @return
     */
    @GetMapping("/webSocket")
    public String webSocket(){
        return "webSocket/socketIndex";
    }
}