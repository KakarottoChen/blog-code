package com.cc.testproject.threadPool.async;

import cn.hutool.core.util.StrUtil;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/** 执行异步的类
 * @Description
 * @Author CC
 * @Date 2022/1/18
 * @Version 1.0
 */
@Service
@Async
public class AsyncServiceImpl implements AsyncService{

    /** 原子整型类的使用 */
    AtomicInteger countInt = new AtomicInteger(0);
    /** 使变量在多个线程之间是可见的，但是不具备原子性 */
    volatile int countVo = 1;
    /** 普通变量 */
    int countPt = 10;

    /** 异步方法1
     */
    @Override
    public void async01(){
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(StrUtil.format("异步01执行，原子整型计数：{}。vol计数：{}。普通变量计数：{}。线程：{}",
                countInt.addAndGet(1),
                countVo++,
                countPt++,
                Thread.currentThread().getName()));
    }

    /** 异步方法2
     */
    @Override
    public void async02(){
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(StrUtil.format("异步02执行，原子整型计数：{}。vol计数：{}。普通变量计数：{}。线程：{}",
                countInt.addAndGet(1),
                countVo++,
                countPt++,
                Thread.currentThread().getName()));
    }

}
