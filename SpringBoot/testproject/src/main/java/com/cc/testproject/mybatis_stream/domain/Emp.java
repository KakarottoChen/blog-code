package com.cc.testproject.mybatis_stream.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description
 * @Author CC
 * @Date 2022/1/18
 * @Version 1.0
 */
@Data
@TableName("emp")
public class Emp implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableField("name")
    private String name;

}
