package com.cc.testproject.activiti7.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description 实现序列化（implements Serializable）原因：如果将 pojo 存储到流程变量中，必须实现序列化接口 serializable，
 *                                                      为了防止由于新增字段无法反序列化，需要生成 serialVersionUID。
 * @Author CC
 * @Date 2021/12/30
 * @Version 1.0
 */
@Data
@NoArgsConstructor
public class Evection implements Serializable {

    public Evection(Double num) {
        this.num = num;
    }

    /**
     * 主键id
     */
    private Long id;
    /**
     * 出差申请单名称
     */
    private String evectionName;
    /**
     * 出差天数（☆）区分走总经理审批还是财务审批的
     */
    private Double num;
    /**
     * 预计开始时间
     */
    private Date beginDate;
    /**
     * 预计结束时间
     */
    private Date endDate;
    /**
     * 目的地
     */
    private String destination;
    /**
     * 出差事由
     */
    private String reson;

}
