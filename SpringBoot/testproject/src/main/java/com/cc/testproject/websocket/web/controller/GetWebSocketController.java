package com.cc.testproject.websocket.web.controller;

import com.cc.testproject.websocket.socket.MyWebSocket;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * @Description
 * @Author CC
 * @Date 2021/11/19
 * @Version 1.0
 */
@RestController
@RequestMapping("/webSocket")
public class GetWebSocketController {

    @Resource
    private MyWebSocket myWebSocket;

    /** 后端给前端发送消息
     * @Description
     * @Author CC
     * @Date 2021/11/19
     * @Param []
     * @return java.lang.String
     **/
    @GetMapping("/sendMessage")
    public void getWebSocket(){
        myWebSocket.sendMessage("ddddddddddd发发发发！");
    }



}
