package com.cc.testproject.threadPool.async;

/**
 * @Description
 * @Author CC
 * @Date 2022/1/18
 * @Version 1.0
 */
public interface AsyncService {

    void async01();

    void async02();

}
