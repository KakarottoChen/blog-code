package com.cc.testproject.web.controller.req;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author CC
 * @since 2022/3/16
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LocalDataTimeReq2 {

    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date date;

    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime localDateTime;

}
