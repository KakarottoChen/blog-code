package com.cc.testproject.web.controller;

import com.cc.testproject.entity.Cs1;
import com.cc.testproject.web.controller.mapper.Cs1Mapper;
import com.cc.testproject.web.controller.req.LocalDataTimeReq;
import com.cc.testproject.web.controller.req.LocalDataTimeReq2;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author CC
 * @since 2022/3/16
 */
@RequestMapping({"/LocalData","localDataTime"})
@RestController
@Slf4j
public class LocalDataTimeController {

    @Resource
    private Cs1Mapper cs1Mapper;

    /** 测试通过
     *  <P>POST请求接收：Date、LocalDateTime</P>
     **/
    @PostMapping
    public Object saveLocalDataTime(@RequestBody LocalDataTimeReq req){
        System.out.println(req);
        //测试通过，可以保存：LocalDateTime 类型的参数
        int insert = cs1Mapper.insert(new Cs1(2L,"名字2",req.getDate(),req.getLocalDateTime()));
        System.out.println("插入数据：" + insert);
        return req;
    }

    /** 测试通过(GET请求1)
     *  <P>GET请求接收：Date、LocalDateTime</P>
     **/
    @GetMapping("/get1")
    public Object getLocalDataTime(LocalDataTimeReq2 req2){
        System.out.println(req2);
        return req2;
    }

    /** 测试通过(GET请求2)
     *  <P>GET请求接收：Date、LocalDateTime</P>
     **/
    @GetMapping("/get2")
    public Object getLocalDataTime(@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss") LocalDateTime localDateTime,
                                   @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss") Date date
    ){
        System.out.println(localDateTime);
        System.out.println(date);
        return new LocalDataTimeReq2(date,localDateTime);
    }


}
