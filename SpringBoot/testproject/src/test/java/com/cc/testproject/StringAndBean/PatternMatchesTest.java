package com.cc.testproject.StringAndBean;

import com.cc.testproject.BaseTest;
import org.junit.Test;

import java.util.regex.Pattern;

/**
 * @Description
 * @Author ChenCheng
 * @Date 2021/8/17 15:32
 * @Version 1.0
 */
public class PatternMatchesTest extends BaseTest {

    /** true：可以大小写字母 + 特殊字符
     * @Description
     * @Author ChenCheng
     * @Date 2021/8/17 17:20
     **/
    @Test
    public void test01() throws Exception {
        String str = "_-`~!@#$%^&*()+=|{}':;',[].<>/?~！@#￥%…AJDKLJaldjflajf…&*（）——+|{}【】‘；：”“’。，、？ddddafdfaADFAF";
        boolean matches = str.matches("^[_\\-`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？A-Za-z]+$");
        System.out.println(matches);
    }

    /** true:只能是数字
     * @Description
     * @Author ChenCheng
     * @Date 2021/8/17 17:20
     **/
    @Test
    public void test02() throws Exception {
        String str = "0004564";
        boolean matches = str.matches("^[0-9]+$");
        System.out.println(matches);
    }

    public static Boolean isNine1(String str){
        String pattern = "^[0,9]+$";
        return Pattern.compile(pattern).matcher(str).matches();
    }
    @Test
    public void test05()throws Exception{
        System.out.println(isNine1("1"));//false
        System.out.println(isNine1("a"));//false
        System.out.println(isNine1("9 0"));//false
        System.out.println(isNine1("90"));       //true
        System.out.println(isNine1("09"));       //true
        System.out.println(isNine1("9"));        //true
        System.out.println(isNine1("99"));       //true
        System.out.println(isNine1(" 999"));//false
        System.out.println(isNine1("999 "));//false
        System.out.println(isNine1("9 99"));//false
    }
    public static Boolean isNine2(String str){
        String pattern = "^[9]+$";
        return Pattern.compile(pattern).matcher(str).matches();
    }
    @Test
    public void test07()throws Exception{
        System.out.println(isNine2("9"));//true
        System.out.println(isNine2("99"));//true
        System.out.println(isNine2("999"));//true
        System.out.println(isNine2("99 9"));//false
        System.out.println(isNine2(" 999"));//false
        System.out.println(isNine2("09"));//false
        System.out.println(isNine2("19"));//false
    }


    //---------------三个方法验证 包含所有数字（正负整数、正负小数）---------------
    public static boolean checkNumber(double value) {
        String str = String.valueOf(value);
        String regex = "^(-?[1-9]\\d*\\.?\\d*)|(-?0\\.\\d*[1-9])|(-?[0])|(-?[0]\\.\\d*)$";
        return str.matches(regex);
    }

    public static boolean checkNumber(int value) {
        String str = String.valueOf(value);
        String regex = "^(-?[1-9]\\d*\\.?\\d*)|(-?0\\.\\d*[1-9])|(-?[0])|(-?[0]\\.\\d*)$";
        return str.matches(regex);
    }

    public static boolean checkNumber(String value) {
        String regex = "^(-?[1-9]\\d*\\.?\\d*)|(-?0\\.\\d*[1-9])|(-?[0])|(-?[0]\\.\\d*)$";
        return value.matches(regex);
    }

    public static void main(String[] args) {
        System.out.println(checkNumber(0));// true
        System.out.println(checkNumber(-0));// true
        System.out.println(checkNumber(23));// true
        System.out.println(checkNumber(-23));// true
        System.out.println(checkNumber(0.0));// true
        System.out.println(checkNumber(-0.0));// true
        System.out.println(checkNumber(23.01));// true
        System.out.println(checkNumber(-23.01));// true
        System.out.println(checkNumber("-10.01.01"));// false
        System.out.println(checkNumber("A110"));// false
    }
//---------------三个方法验证 包含所有数字（正负整数、正负小数）---------------

    /** true：只要有数字都可以：一个方法，验证所有的数字（正负整数、正负小数）+只要有数字，都是true
     * @Description
     * @Author ChenCheng
     * @Date 2021/8/17 17:17
     **/
    @Test
    public void test03() throws Exception {
        String pattern = ".*\\d+.*";
        Pattern compile = Pattern.compile(pattern);
        System.out.println(compile.matcher("-2.3").matches());// true
        System.out.println(compile.matcher("2.3").matches());// true
        System.out.println(compile.matcher("23").matches());// true
        System.out.println(compile.matcher("-45").matches());// true
        System.out.println(compile.matcher(
                "打法8/-*/……%&%￥%*￥*……@@！*~~··/2").matches());// true
        System.out.println(compile.matcher(
                "打法2//*-/*8").matches());// true
        System.out.println(compile.matcher(
                "打法2//*锻炼就好啦觉得很-/*8").matches());// true

    }

    /** true: 只能是整数
     * @Description
     * @Author ChenCheng
     * @Date 2021/8/18 11:23
     * @Param [str]
     * @return boolean
     **/
    public static boolean isInteger(String str) {
        Pattern pattern = Pattern.compile("^[-\\+]?[\\d]*$");
        return pattern.matcher(str).matches();
    }

    @Test
    public void test04()throws Exception{
        System.out.println(isInteger(""));
        System.out.println(isInteger("2"));
        System.out.println(isInteger("a"));
        System.out.println(isInteger("c"));
    }

    @Test
    public void test06()throws Exception{
        String param = "CC";
        String str = "CC0";
        char c = str.charAt(param.length());
        System.out.println(c);
        if (c == '0'){
            System.out.println("判断成功!");
        }else {
            System.out.println("失败！");
        }
    }

}
