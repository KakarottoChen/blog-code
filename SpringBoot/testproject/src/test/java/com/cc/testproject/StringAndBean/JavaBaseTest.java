package com.cc.testproject.StringAndBean;

import com.cc.testproject.BaseTest;
import org.junit.Test;

import java.util.Objects;

/** java基础测试
 * @Description
 * @Author ChenCheng
 * @Date 2021/7/31 10:29
 * @Version 1.0
 */
public class JavaBaseTest extends BaseTest {

    /** 测试 Objects.isNull
     * @Description 是null，返回ture
     * @Author ChenCheng
     * @Date 2021/7/31 10:29
     **/
    @Test
    public void test01()throws Exception{
        String s1 = null;
        String s2 = "";
        String s3 = " ";
        Double d1 = null;
        System.out.println(Objects.isNull(s1));
        System.out.println(Objects.isNull(s2));
        System.out.println(Objects.isNull(s3));
        System.out.println(Objects.isNull(d1));
    }

    /** 测试 Objects.nonNull
     * @Description 是null，返回false
     * @Author ChenCheng
     * @Date 2021/7/31 11:23
     **/
    @Test
    public void test02()throws Exception{
        String s1 = null;
        String s2 = "";
        String s3 = " ";
        Double d1 = null;
        System.out.println(Objects.nonNull(s1));
        System.out.println(Objects.nonNull(s2));
        System.out.println(Objects.nonNull(s3));
        System.out.println(Objects.nonNull(d1));
    }

    @Test
    public void test03()throws Exception{
        int a = 255;//总条数
        int b = 90;//每页条数
        System.out.println(a/b);//2.833333
        System.out.println(a%b);
    }
}
