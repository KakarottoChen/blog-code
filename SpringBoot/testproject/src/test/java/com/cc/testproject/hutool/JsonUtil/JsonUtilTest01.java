package com.cc.testproject.hutool.JsonUtil;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.cc.testproject.BaseTest;
import org.junit.Test;

/**
 * @Description
 * @Author CC
 * @Date 2022/2/22
 * @Version 1.0
 */
public class JsonUtilTest01 extends BaseTest {


    /** 拼凑json
     * 结果：
     *      {
     *          "filter1": {
     *              "data": "data1",
     *              "data2": "data2"
     *          },
     *          "filter2": "haha2"
     *      }
     * @throws Exception
     */
    @Test
    public void test01()throws Exception{
        JSONObject param1 = JSONUtil.createObj();
        JSONObject param2 = JSONUtil.createObj();
        JSONObject param3 = param2.set("data", "data1").set("data2", "data2");
        System.out.println(param3);
        param1.set("filter1",param2)
                .set("filter2","haha2");
        System.out.println(JSONUtil.toJsonStr(param1));

        JSONObject param = JSONUtil.createObj().set(
                "filter", JSONUtil.createObj().set("taskId", "33jjdjdjdjdjdjaakakakka"));
        System.out.println(param);
    }

}
