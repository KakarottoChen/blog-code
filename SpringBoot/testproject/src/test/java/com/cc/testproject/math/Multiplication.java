package com.cc.testproject.math;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;

import java.util.*;

/** 乘法
 * @Description
 * @Author ChenCheng
 * @Date 2021/8/26 15:06
 * @Version 1.0
 */
public class Multiplication {

    /** 两种实现阶乘的方法
     * @Description
     * @Author ChenCheng
     * @Date 2021/8/26 15:06
     **/
    //1采用循环连乘法
    public static int fact(int num){
        int temp=1;
        int factorial=1;
        while(num>=temp){
            factorial=factorial*temp;
            temp++;
        }
        return factorial;
    }
    //2采用递归法
    public static int recurrence(int num){
        if(num<=1){
            return 1;
        } else{
            return num*recurrence(num-1);
        }
    }
    @Test
    public void test1阶乘()throws Exception{
        System.out.println("阶乘：循环连乘法:"+fact(6));
        System.out.println("阶乘：递归法:"+recurrence(6));
    }

    @Test
    public void test02()throws Exception{
        List<Integer> a = Arrays.asList(1, 2);
        List<Integer> b = Arrays.asList(2, 3);
        List<Integer> c = Lists.newArrayList();
        c.addAll(a);
        c.addAll(b);
        Set<Integer> set = new HashSet<>(c);
        System.out.println(set);
    }

    public String doSomeBiz(int input){
        return input == 1 ? "one" : input == 2 ? "two" : input == 3 ? "three" : "three+";
    }
    @Test
    public void test01()throws Exception{
        System.out.println(doSomeBiz(1));
        System.out.println(doSomeBiz(2));
        System.out.println(doSomeBiz(3));
        System.out.println(doSomeBiz(4));
        System.out.println(doSomeBiz(5));
    }

}
