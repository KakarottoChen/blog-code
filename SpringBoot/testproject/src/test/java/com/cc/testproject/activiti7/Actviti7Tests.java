/*
package com.cc.testproject.activiti7;

import cn.hutool.core.util.StrUtil;
import com.cc.testproject.BaseTest;
import com.cc.testproject.activiti7.securityConfigUtil.SecurityUtil;
import lombok.extern.slf4j.Slf4j;
import org.activiti.api.process.runtime.ProcessRuntime;
import org.activiti.api.runtime.shared.query.Page;
import org.activiti.api.runtime.shared.query.Pageable;
import org.activiti.api.task.model.payloads.CompleteTaskPayload;
import org.activiti.api.task.runtime.TaskRuntime;
import org.activiti.engine.*;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.history.HistoricActivityInstanceQuery;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.repository.ProcessDefinitionQuery;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.List;
import java.util.zip.ZipInputStream;

*/
/**
 *//*

@Slf4j
public class Actviti7Tests extends BaseTest {

    @Autowired
    private ProcessRuntime processRuntime;
    @Autowired
    private TaskRuntime taskRuntime;
    @Autowired
    private SecurityUtil securityUtil;

    */
/** 部署：手动创建引入流程bpmn文件
     *
     *//*

    @Test
    public void test01()throws Exception{
//        securityUtil.logInAs("zhangsan");
        //1、创建ProcessEngine
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        //2、得到RepositoryService实例 :流程定义和部署存储库的访问的服务
        RepositoryService repositoryService = processEngine.getRepositoryService();
        //3、使用RepositoryService进行部署
        Deployment deployment = repositoryService.createDeployment()
                .addClasspathResource("bpmn/myEveition.bpmn20.xml") // 添加bpmn资源
                .addClasspathResource("bpmn/myEveition.png") // 添加png资源
                .name("出差流程")
                .deploy();
        //4、输出部署信息
        System.out.println(StrUtil.format("任务id：",deployment.getId()));
        System.out.println(StrUtil.format("任务名字：{}",deployment.getName()));
        System.out.println(StrUtil.format("租户id：{}",deployment.getTenantId()));
        System.out.println(StrUtil.format("部署时间：{}",deployment.getDeploymentTime()));
        System.out.println(StrUtil.format("类别：{}",deployment.getCategory()));
        System.out.println(StrUtil.format("key：{}",deployment.getKey()));

    }

    */
/** 部署：自动部署流程，前提是：bpmn文件要放在 resources/processes下
     *    会自动生成表
     *    自动把流程注入到数据库的表中
     *//*

    @Test
    public void test02()throws Exception{
        //确认登陆人是谁
        securityUtil.logInAs("jack");
        //流程定义分页对象
        Page<org.activiti.api.process.model.ProcessDefinition> processDefinitionPage = processRuntime.processDefinitions(Pageable.of(0, 10));
        List<org.activiti.api.process.model.ProcessDefinition> pds = processDefinitionPage.getContent();
        log.info("可用的流程定义总数{}",pds.size());
        //循环得到一个个流程
        for (org.activiti.api.process.model.ProcessDefinition pd : processDefinitionPage.getContent()) {
            log.info("流程定义的内容：{}",pd);
        }
    }

    */
/**
     * 启动流程实例
     *//*

    @Test
    public void test03()throws Exception{
        //确认登陆人是谁
//        securityUtil.logInAs("zhangsan");
        //1、创建ProcessEngine
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        //2、获取RunTimeService
        RuntimeService runtimeService = processEngine.getRuntimeService();
        //3、根据流程定义Id启动流程
        ProcessInstance processInstance = runtimeService
//                .startProcessInstanceById("myEveition2:1:deaa6db5-62d5-11ec-b121-a0510b6c253b"); //通过流程定义id启动流程
                .startProcessInstanceByKey("myEveition2"); //通过流程定义key启动流程
        //4、输出内容
        System.out.println(StrUtil.format("流程实例id：",processInstance.getId()));
        System.out.println(StrUtil.format("流程定义ID：{}",processInstance.getProcessDefinitionId()));
        System.out.println(StrUtil.format("当前活动Id：",processInstance.getActivityId()));
    }

    */
/**
     * 查询当前个人待执行的任务
     *//*

    @Test
    public void test04()throws Exception{
        //任务负责人
        String assignee = "zhangsan";
        //确认登陆人是谁
        securityUtil.logInAs(assignee);
        //1、创建ProcessEngine
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        //2、获取taskService ：操作任务的表
        TaskService taskService = processEngine.getTaskService();
        //3.根据流程key 和 任务负责人 查询任务
        List<Task> tasks = taskService.createTaskQuery()
//                .processDefinitionKey("myEveition")
                .taskDefinitionKey("Activity_0hdovod")
                .taskAssignee(assignee)
                .list();
        //查看所有任务
        for (Task task : tasks) {
            System.out.println("流程实例id：" + task.getProcessInstanceId());
            System.out.println("任务id：" + task.getId());
            System.out.println("任务负责人：" + task.getAssignee());
            System.out.println("任务负责人的任务定义key：" + task.getTaskDefinitionKey());
            System.out.println("任务名称：" + task.getName());
        }
    }

    */
/**
     * 完成任务
     *//*

    @Test
    public void completTask(){
        String pep = "jack";
//        获取引擎
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
//        获取taskService
        TaskService taskService = processEngine.getTaskService();
        HistoryService historyService = processEngine.getHistoryService();
//        根据流程key 和 任务的负责人 查询任务
//        返回一个任务对象
        Task task = taskService.createTaskQuery()
//                .processDefinitionKey("Activity_0gyay79") //流程Key
                .taskAssignee(pep)  //要查询的负责人
                .singleResult();
        List<HistoricTaskInstance> list = historyService.createHistoricTaskInstanceQuery()
                .finished()
                .list();

        List<Task> jackTask = taskService.createTaskQuery().list();
//        完成任务,参数：任务id
        taskService.complete(jackTask.get(0).getId());
        //确认登陆人是谁
        securityUtil.logInAs(pep);

        Page<org.activiti.api.task.model.Task> page = taskRuntime.tasks(Pageable.of(0, 10));
        List<org.activiti.api.task.model.Task> tasks = page.getContent();
        tasks.forEach(task1 -> {
            String id1 = task1.getId();
            CompleteTaskPayload cp1 = new CompleteTaskPayload();
            cp1.setTaskId(id1);
            System.out.println("任务id："+id1);
            taskRuntime.complete(cp1);
        });
    }

    */
/**
     * zip方式部署
     *//*

    @Test
    public void test05()throws Exception{
        //获取引擎
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        //获取 repositoryService
        RepositoryService repositoryService = processEngine.getRepositoryService();
        //获取inputStream输入流 —— 获取这个类，获取这个项目，获取这个项目的Resource目录，从目录中读取zip包
        InputStream inputStream = this.getClass()
                .getClassLoader()
                .getResourceAsStream("bpmn/myEveitions.zip");
        //构建ZipInputStream流
        ZipInputStream zipInputStream = new ZipInputStream(inputStream);
        //使用zip包的方式部署
        Deployment deploy = repositoryService
                .createDeployment()
                .addZipInputStream(zipInputStream)
                .deploy();
        //输出
        System.out.println("id:"+deploy.getId());
        System.out.println("name:"+deploy.getName());
    }

    */
/**
     * 查询流程定义:就是获取所有已经定义了的流程
     *//*

    @Test
    public void test06()throws Exception{
        //获取引擎
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        //获取 repositoryService
        RepositoryService repositoryService = processEngine.getRepositoryService();
        //得到ProcessDefinitionQuery 对象
        ProcessDefinitionQuery processDefinitionQuery = repositoryService.createProcessDefinitionQuery();
        //查询出当前所有的流程定义
        List<org.activiti.engine.repository.ProcessDefinition> myEveitions = processDefinitionQuery
//                .processDefinitionKey("myEveition") //通过流程定义key获取流程，注释了，就查询所有的流程定义
                .orderByProcessDefinitionVersion() //按版本排序
                .desc() //倒叙
                .list(); //获取集合
        //输出流程定义信息
        for (org.activiti.engine.repository.ProcessDefinition processDefinition : myEveitions) {
            System.out.println("流程定义 id="+processDefinition.getId());
            System.out.println("流程定义 name="+processDefinition.getName());
            System.out.println("流程定义 key="+processDefinition.getKey());
            System.out.println("流程定义 Version="+processDefinition.getVersion());
            System.out.println("流程部署ID ="+processDefinition.getDeploymentId());
        }
    }

    */
/**
     * 删除流程定义
     *//*

    @Test
    public void test07()throws Exception{
        //获取引擎
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        //获取 repositoryService
        RepositoryService repositoryService = processEngine.getRepositoryService();
        //获取流程定义id
        String deploymentId = "de8ef672-62d5-11ec-b121-a0510b6c253b";
//        String deploymentId = "97c21b06-62d1-11ec-93f8-a0510b6c253b";
//        String deploymentId = "7f0c1dfe-62d1-11ec-af06-a0510b6c253b";
        //删除：是否级联删除：级联删除，如果该流程有正在执行的任务，也会被删除 —— 经测试，历史表也被删除了。
//        repositoryService.deleteDeployment(deploymentId,false);
        repositoryService.deleteDeployment(deploymentId,true);
    }

    */
/**
     * 下载流程文件
     *//*

    @Test
    public void test08()throws Exception{
        //1、得到引擎
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        //2、获取repositoryService
        RepositoryService repositoryService = processEngine.getRepositoryService();
        //3、得到查询器：ProcessDefinitionQuery，设置查询条件,得到想要的流程定义
        ProcessDefinition definition = repositoryService
                .createProcessDefinitionQuery()
                .orderByProcessDefinitionVersion().desc() //按照版本号倒序排序
                .processDefinitionKey("myEveition2").singleResult();//通过key获取部署了的流程
        //4、通过流程定义信息，得到部署ID
        String deploymentId = definition.getDeploymentId();
        //5、通过repositoryService的方法，实现读取图片信息和bpmn信息
            //png图片的流
        String pngName = definition.getDiagramResourceName();
        InputStream pngInput = repositoryService.getResourceAsStream(deploymentId, pngName);
            //bpmn文件的流
        String bpmnName = definition.getResourceName();
        InputStream bpmnInput = repositoryService.getResourceAsStream(deploymentId, bpmnName);
        //6、构造OutputStream流
        File pngFile = new File("E:\\itwen-dang2\\SpringBoot\\testcode\\testproject\\doc\\activiti7\\myeve.png");
        FileOutputStream pngOut = new FileOutputStream(pngFile);
        File bpmnFile = new File("E:\\itwen-dang2\\SpringBoot\\testcode\\testproject\\doc\\activiti7\\myeve.bpmn");
        FileOutputStream bpmnOut = new FileOutputStream(bpmnFile);
        //7、输入流，输出流的转换
        IOUtils.copy(pngInput,pngOut);
        IOUtils.copy(bpmnInput,bpmnOut);
        //8、关闭流
        pngOut.close();
        bpmnOut.close();
        pngInput.close();
        bpmnInput.close();
        //9、可以扩展成，选择下载位置
    }

    */
/**
     * 查询历史记录
     * @throws Exception
     *//*

    @Test
    public void test09()throws Exception{
//      获取引擎
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
//        获取HistoryService
        HistoryService historyService = processEngine.getHistoryService();
//        获取 actinst表的查询对象
        HistoricActivityInstanceQuery instanceQuery = historyService.createHistoricActivityInstanceQuery();
//        查询 actinst表，条件：根据 InstanceId 查询
        instanceQuery.processInstanceId("adce09cb-66f3-11ec-a9b7-a0510b6c253b");
//        查询 actinst表，条件：根据 DefinitionId 查询
//        instanceQuery.processDefinitionId("myEvection:1:4");
//        增加排序操作,orderByHistoricActivityInstanceStartTime 根据开始时间排序 asc 升序
        instanceQuery.orderByHistoricActivityInstanceStartTime().asc();
//        查询所有内容
        List<HistoricActivityInstance> activityInstanceList = instanceQuery.list();
//        输出
        for (HistoricActivityInstance hi : activityInstanceList) {
            System.out.println(hi.getActivityId());
            System.out.println(hi.getActivityName());
            System.out.println(hi.getProcessDefinitionId());
            System.out.println(hi.getProcessInstanceId());
            System.out.println("<==========================>");
        }
    }

}
*/
