package com.cc.testproject.thread;

import cn.hutool.core.thread.ExecutorBuilder;
import cn.hutool.core.thread.ThreadFactoryBuilder;
import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.core.util.StrUtil;
import com.cc.testproject.BaseTest;
import com.cc.testproject.threadPool.config.MyThreadPool;
import com.google.common.collect.Lists;
import org.junit.Test;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.*;

/** 测试自定义线程池
 * @Description
 * @Author CC
 * @Date 2022/1/17
 * @Version 1.0
 */
public class MyThreadPoolTest extends BaseTest {

    @Resource
    private MyThreadPool myThreadPool;

    /** CompletableFuture.supplyAsync  表示创建【带返回值】的异步任务
     *  相当于ExecutorService submit(Callable<T> task) 方法
     *  效果跟submit是一样
     *
     *  异步
     */
    @Test
    public void test03()throws Exception{
        List<Integer> list = Lists.newArrayList(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15);
        list.forEach(l -> {
            CompletableFuture<Integer> cf = CompletableFuture.supplyAsync(() -> {
                System.out.println(l + ":当前线程" + Thread.currentThread().getName());
                return l;
            }, myThreadPool.threadPoolTaskExecutor());
            //回调
            cf.thenApply(result -> {
                System.out.println(1);
                return result * 4;
            });
            ThreadFactoryBuilder threadBuild = ThreadFactoryBuilder.create();
            ExecutorBuilder executorBuilder = ExecutorBuilder.create();
            ExecutorService executorService = ThreadUtil.newExecutor(8);

            CompletableFuture.supplyAsync(() -> {
                return null;
            });

            try {
                //等待子线程执行完，再执行主线程
                System.out.println("返回值：" + cf.get());
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        });
        System.out.println("总线程：" + Thread.currentThread().getName());
    }

    /** CompletableFuture.runAsync  表示创建【无返回值】的异步任务
     *  相当于ExecutorService submit(Runnable task)方法
     *  效果跟submit是一样
     */
    @Test
    public void test02()throws Exception{
        List<Object> list = Lists.newArrayList(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15);
        //把list分片，就相当于开多少个线程去执行任务
        list.forEach(l -> {
            CompletableFuture<Void> cm = CompletableFuture.runAsync(
                    () -> {
                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        System.out.println("当前线程" + Thread.currentThread().getName());
                        System.out.println("Runnable执行：" + l);
                        System.out.println("--------------------------------------------------");
                    },
                    myThreadPool.threadPoolTaskExecutor());
            //线程按照顺序执行
//            cm.join();
            try {
                //等待子线程执行完，再执行主线程
                System.out.println(cm.get());
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        });
        System.out.println("总线程：" + Thread.currentThread().getName());
    }

    /** 直接使用线程池
     */
    @Test
    public void test01()throws Exception{
        //获取线程池
        ThreadPoolTaskExecutor executor = myThreadPool.threadPoolTaskExecutor();
        //提交线程1
        executor.execute(new Thread01());
        //提交线程2
        Future<Integer> submit = executor.submit(new Callable01());
        System.out.println("Callable01返回值：" + submit.get());
        executor.submit(() -> {
            System.out.println(11111);
            System.out.println(11111);
            return 3*7;
        });
        //提交线程3
        executor.execute(new Runnable01());
        //1秒后，不管有没有线程正在执行，都关闭线程池
        executor.setAwaitTerminationMillis(1000L);
        //等线程执行完再关闭
        executor.shutdown();
        //主线程执行
        System.out.println("主线程：" + Thread.currentThread().getName());
    }

    class Thread01 extends Thread {
        @Override
        public void run() {
            System.out.println("Thread01执行1");
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Thread01执行2");
        }
    }

    class Callable01 implements Callable<Integer> {
        @Override
        public Integer call() throws Exception {
            System.out.println("Callable01执行");
            return 5 * 8;
        }
    }

    class Runnable01 implements Runnable {
        @Override
        public void run() {
            System.out.println("Runnable01执行");
        }
    }


}
