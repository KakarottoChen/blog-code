package com.cc.testproject.DesignPatterns;

/**单例模式：懒汉
 * @Description
 * @Author ChenCheng
 * @Date 2021/8/26 15:14
 **/
public class SingleTonLazy {

    private static SingleTonLazy singleTon = null;

    private SingleTonLazy() {
    }

    //双重否定 + 同步
    public static SingleTonLazy getInstance() {
        if (singleTon == null) {
            synchronized (SingleTonLazy.class) {
                if (singleTon == null) {
                    singleTon = new SingleTonLazy();
                }
            }
        }
        return singleTon;
    }
}