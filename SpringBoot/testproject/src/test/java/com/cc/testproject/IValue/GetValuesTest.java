package com.cc.testproject.IValue;

import com.cc.testproject.BaseTest;
import lombok.Data;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;

/**
 * @Description
 * @Author CC
 * @Date 2021/12/12
 * @Version 1.0
 */
public class GetValuesTest extends BaseTest {

    @Value("${projectUrl.id}")
    private String id;
    @Value("${projectUrl.name}")
    private String name;
    @Value("${projectUrl.url}")
    private String url;

    public static String idd = "";

    @Test
    public void test01()throws Exception{
        System.out.println(id);
        System.out.println(name);
        System.out.println(url);
        idd = getId();
        System.out.println(idd);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
