package com.cc.testproject.thread;

import com.cc.testproject.BaseTest;
import org.junit.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/** 创建系统的4个自带线程池
 * 1、线程池调用方式：
 *   execute()  发生异常直接抛出，只能运行 Runnable
 *   submit()   发生异常不会抛出，
 *                Callable需要Tuture.get()获取返回值，才会抛出异常
 * 2、关闭线程池方式：
 *   shutdownNow() 线程池拒接收新提交的任务，同时⽴刻关闭线程池，线程池里的任务不再执行。
 *                   当我们使⽤shutdownNow⽅法关闭线程池时，一定要对任务里进行异常捕获。
 *   shuwdown()    线程池拒接收新提交的任务，同时等待线程池⾥的任务执行完毕后关闭线程池。 —— 用这种
 *                   当我们使用shuwdown方法关闭线程池时，一定要确保任务里不会有永久阻塞等待的逻辑，否则线程池就关闭不了。
 *     threadPool.awaitTermination(5, TimeUnit.SECONDS);
 *       关闭线程池，线程池不一定马上关闭，所以需要使用
 *       意思是5秒后，不管还有没有线程执行，都要关闭线程池。
 *     threadPool.isShutdown()  线程是否关闭
 */
public class ThreadPoolTest extends BaseTest {

    /**定长线程池
     *    最小线程池数 = 最大线程数
     *    适合执行【长期的任务】
     * 队列：LinkedBlockingQueue
     *
     */
    @Test
    public void test01()throws Exception{
        //定长线程池
        ExecutorService threadPool = Executors.newFixedThreadPool(5);
        //创建有返回值的线程
        CallableTask callable = new CallableTask();
        //线程池提交线程
        Future<Integer> submit = threadPool.submit(callable);
        //获取返回值，输出
        Integer integer = submit.get();
        System.out.println("定长线程池返回值："+integer);
        //关闭线程池
        System.out.println("线程池是否关闭1："+threadPool.isShutdown());
        //第二个线程执行
        Thread thread = new Thread(() -> {
            System.out.println("线程2执行1");
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("线程2执行2");
        });
        threadPool.submit(thread);
        //关闭线程池
        threadPool.shutdown();
        //使用场景：在使用Executors线程池时，有时场景需要【主线程等各子线程都运行完毕后再执行】。
        //与shutdown()方法结合使用时，尤其要注意的是shutdown()方法必须要在awaitTermination()方法之前调用，该方法才会生效。否则会造成死锁。
        //7秒后不论线程执行完没有，都要关闭线程池
        boolean isTermination = threadPool.awaitTermination(7, TimeUnit.SECONDS);
        System.out.println("是否全部执行完：" + isTermination);
        System.out.println("线程池是否关闭2：" + threadPool.isShutdown());
    }

    // -------------------定长线程池-----------------------
    @Test
    public void test02()throws Exception{

    }



}
