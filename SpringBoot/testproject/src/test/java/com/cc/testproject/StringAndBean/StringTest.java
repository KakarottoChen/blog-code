package com.cc.testproject.StringAndBean;

import com.cc.testproject.BaseTest;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.springframework.util.Assert;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Description
 * @Author CC
 * @Date 2021/10/30
 * @Version 1.0
 */
public class StringTest extends BaseTest {

    //把传入的所有字符串，用“-”符号隔开
    @Test
    public void test01Join()throws Exception{
        String join = String.join("-", "abc","def");
        System.out.println(join);//abc-def
    }

    @Test
    public void test02()throws Exception{
        String ss = "2022-01-12 15:10:43.000000";
        System.out.println(ss.substring(0, ss.length() - 7));
    }
    private volatile int index = 0;
    @Test
    public void test03()throws Exception{
        String str = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa123456789";
        System.out.println(str.getBytes().length);
        System.out.println(StringUtils.isBlank(""));
        System.out.println(StringUtils.isBlank(null));
//        Assert.notNull(null,"laile");

        AtomicInteger aci = new AtomicInteger(index);
        index = aci.addAndGet(1);
        System.out.println(index);
    }




}
