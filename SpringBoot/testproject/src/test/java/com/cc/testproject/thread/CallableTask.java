package com.cc.testproject.thread;

import java.util.concurrent.Callable;

/** 创建线程：Callable
 * @Description
 * @Author CC
 * @Date 2021/11/3
 * @Version 1.0
 */
public class CallableTask implements Callable<Integer> {

    //线程需要执行的任务
    @Override
    public Integer call() {
        System.out.println("执行线程Callable");
        System.out.println("定长线程池线程名字："+Thread.currentThread().getName());
        /*try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
        return 5*6;
    }
}
