package com.cc.testproject.time_date;

import org.junit.Test;
import org.springframework.util.Assert;

import java.util.Calendar;

/** 月历测试
 * @Description
 * @Author ChenCheng
 * @Date 2021/8/10 19:24
 * @Version 1.0
 */
public class CalendarTest {

    @Test
    public void test01()throws Exception{
        //获取日历对象
        Calendar calendar = Calendar.getInstance();
        //获取当前月份的最后一天的日期
        int actualMaximum = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        System.out.println(actualMaximum);
        //获取当前月份的当前天的天数
        int today = calendar.get(Calendar.DAY_OF_MONTH);
        System.out.println(today);
        //获取当前周的周几
        int week = calendar.get(Calendar.DAY_OF_WEEK);
        System.out.println("本周几："+(week - 1));
        int monthDay = calendar.get(Calendar.DAY_OF_MONTH);
        System.out.println("本月几号："+monthDay);
        int dayOfYear = calendar.get(Calendar.DAY_OF_YEAR);
        System.out.println("本年第几天："+dayOfYear);
    }

    @Test
    public void test02()throws Exception{
        Integer integer = 4;
        Assert.isTrue( integer >= 1 && integer <= 31,"月份的天只能是1-31");
        Assert.isTrue(integer >= 1 && integer <= 7,"星期只能是1-7");
    }

}
