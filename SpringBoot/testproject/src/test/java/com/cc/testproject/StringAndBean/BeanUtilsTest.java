package com.cc.testproject.StringAndBean;

import com.cc.testproject.BaseTest;
import com.cc.testproject.entity.Employee;
import com.cc.testproject.entity.User;
import com.cc.testproject.util.BeanUtilCc;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.springframework.beans.BeanUtils;

import java.util.List;

/** 测试 BeanUtils 拷贝静态属性
 * @Description
 * @Author ChenCheng
 * @Date 2021/8/12 16:43
 * @Version 1.0
 */
public class BeanUtilsTest extends BaseTest {

    Employee employee;
    Employee employee1;

    {
        employee = new Employee("cc", 18, "部门");
        employee1 = new Employee("dd", 19, "部门");
    }

    /** 拷贝属性给对象
     * @Description
     *  BeanUtils.copyProperties(Object source, Object target)
     *      source:有数据的对象
     *      target:目标对象(拷进去)
     * @Author ChenCheng
     * @Date 2021/8/12 16:45
     **/
    @Test
    public void test01()throws Exception{
        User user = new User();
        User user1 = new User();
        BeanUtils.copyProperties(employee,user);
        BeanUtils.copyProperties(employee1,user1);
        System.out.println(user);
        System.out.println(user1);
    }

    /** 拷贝 List
     * @Description
     * @Author ChenCheng
     * @Date 2021/8/12 16:47
     **/
    @Test
    public void test02()throws Exception{
        List<Employee> emps = Lists.list(employee, employee1);
        List<User> users = BeanUtilCc.convertList2List(emps, User.class);
        System.out.println(users);
    }




}
