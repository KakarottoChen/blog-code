package com.cc.testproject.util;

import cn.hutool.core.bean.BeanUtil;
import org.apache.commons.collections.CollectionUtils;
import org.assertj.core.util.Lists;
import org.springframework.beans.BeanUtils;

import java.util.List;

/**
 * @Description
 * @Author ChenCheng
 * @Date 2021/8/12 17:00
 * @Version 1.0
 */
public class BeanUtilCc {

    /** 拷贝集合中的对象的所有属性给另一个对象
     * @param input 输入集合
     * @param clzz  输出集合类型
     * @param <E>   输入集合类型
     * @param <T>   输出集合类型
     * @return 返回集合
     */
    public static <E, T> List<T> convertList2List(List<E> input, Class<T> clzz) {
        List<T> output = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(input)) {
            for (E source : input) {
                T target = BeanUtils.instantiate(clzz);
                BeanUtil.copyProperties(source, target);
                output.add(target);
            }
        }
        return output;
    }
}
