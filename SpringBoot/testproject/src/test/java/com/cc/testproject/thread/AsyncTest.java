package com.cc.testproject.thread;

import cn.hutool.core.util.StrUtil;
import com.cc.testproject.BaseTest;
import com.cc.testproject.threadPool.async.AsyncService;
import org.junit.Test;

import javax.annotation.Resource;

/**
 * @Description
 * @Author CC
 * @Date 2022/1/18
 * @Version 1.0
 */
public class AsyncTest extends BaseTest {

    @Resource
    private AsyncService asyncService;

    @Test
    public void test01()throws Exception{
        long start = System.currentTimeMillis();
        //开启异步1,两个
        for (int i = 0; i < 3; i++) {
            asyncService.async01();
            System.out.println("主线程开启01。第几次：" + i);
        }
        //开启异步2,三个
        for (int i = 0; i < 3; i++) {
            asyncService.async02();
            System.out.println("主线程开启02。第几次：" + i);
        }
        System.out.println(StrUtil.format("总线程:{}执行完，耗时：{}ms",
                Thread.currentThread().getName(),System.currentTimeMillis() - start));
    }


}
