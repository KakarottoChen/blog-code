package com.cc.testproject.DesignPatterns.test;

import com.cc.testproject.DesignPatterns.SingleTonHunger;
import com.cc.testproject.DesignPatterns.SingleTonLazy;
import org.junit.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/** 单例模式
 * @Description
 * @Author ChenCheng
 * @Date 2021/8/26 15:12
 * @Version 1.0
 */
public class SingleTonTest {

    //创建不同的线程，来调用单例模式
    @Test
    public void test01懒汉模式()throws Exception{
        Thread thread1 = new Thread(() -> {
            String threadName = Thread.currentThread().getName();
            System.out.println(threadName + "：懒汉 单例模式1:" + SingleTonLazy.getInstance());
        });
        //运行线程
        thread1.start();
        //保证线程执行 顺序性
        thread1.join();

        Thread thread2 = new Thread(() -> {
            String threadName = Thread.currentThread().getName();
            System.out.println(threadName + "：懒汉 单例模式2:" + SingleTonLazy.getInstance());
        });
        thread2.start();
        thread2.join();

        Thread thread3 = new Thread(() -> {
            String threadName = Thread.currentThread().getName();
            System.out.println(threadName + "：懒汉 单例模式3:" + SingleTonLazy.getInstance());
        });
        thread3.start();
        thread3.join();

        Thread thread4 = new Thread(() -> {
            String threadName = Thread.currentThread().getName();
            System.out.println(threadName + "：懒汉 单例模式4:" + SingleTonLazy.getInstance());
        });
        thread4.start();
        thread4.join();

        Thread thread5 = new Thread(() -> {
            String threadName = Thread.currentThread().getName();
            System.out.println(threadName + "：懒汉 单例模式5:" + SingleTonLazy.getInstance());
        });
        thread5.start();
        thread5.join();
    }

    @Test
    public void test02饿汉模式()throws Exception{
        Thread thread1 = new Thread(() -> System.out.println("饿汉单例模式1:" + SingleTonHunger.getInstance() + 1/0));
//        System.out.println("thread1："+thread1.getName());
//        thread1.start();

        Thread thread2 = new Thread(() -> System.out.println("饿汉单例模式2:" + SingleTonHunger.getInstance() + 1/0));
//        System.out.println("thread2："+thread2.getName());
//        thread2.start();

        Thread thread3 = new Thread(() -> System.out.println("饿汉单例模式3:" + SingleTonHunger.getInstance()));
//        System.out.println("thread3："+thread3.getName());
//        thread3.start();

        Thread thread4 = new Thread(() -> System.out.println("饿汉单例模式4:" + SingleTonHunger.getInstance()));
//        System.out.println("thread4："+thread4.getName());
//        thread4.start();

        Thread thread5 = new Thread(() -> System.out.println("饿汉单例模式5:" + SingleTonHunger.getInstance()));
//        System.out.println("thread5："+thread5.getName());
//        thread5.start();

        //单线程线程池：保证执行的顺序性
//        ExecutorService executor = Executors.newSingleThreadExecutor();//单例线程池
        ExecutorService executor = Executors.newFixedThreadPool(5);//定长线程池
//        executor.submit(thread1);
        /**
         * 线程池调用方式：
         *    execute()  //发送异常直接抛出
         *    submit()   //发送异常不会抛出，需要Tuture.get()获取返回值，才会抛出异常
         */

        executor.execute(thread1);
        Future<?> submit = executor.submit(thread2);
        submit.get();//调用了该方法，就会抛出异常
        System.out.println("submit是："+submit);

        executor.submit(thread3);
        executor.submit(thread4);
        executor.submit(thread5);
        executor.shutdown();
    }

}
