package com.cc.testproject.Collections;

import cn.hutool.core.util.StrUtil;
import com.cc.testproject.BaseTest;
import org.junit.Test;
import org.springframework.util.CollectionUtils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

/**
 * @author CC
 * @since 2022/5/13 0013
 */
public class ArrayTest extends BaseTest {

    @Test
    public void test01()throws Exception{
        List<String> list1 = Arrays.asList("1", "2", "3");
        List<String> list2 = Arrays.asList("1", "3");
        HashSet<String> set1 = new HashSet<>(list1);
        HashSet<String> set2 = new HashSet<>(list2);
        boolean b = set1.removeAll(set2);
        System.out.println(b);
        System.out.println(set1);
        if (CollectionUtils.isEmpty(list1)){
            System.out.println("空的1？");
        }
        if (list1.size() == 0){
            System.out.println("空的2？");
        }

        String s = "1234";
        System.out.println(StrUtil.subSuf(s, s.length() - 3));

    }


}
