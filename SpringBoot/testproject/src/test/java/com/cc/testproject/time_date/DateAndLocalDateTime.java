package com.cc.testproject.time_date;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.cc.testproject.BaseTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/** LocalDateTime 和 Date 测试
 */
@Slf4j
public class DateAndLocalDateTime extends BaseTest {

    public static final DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    /** 字符串时间转 时间
     *  LocalDateTime 和 Date 获取各自的毫秒数
     * @Description
     * @Author ChenCheng
     * @Date 2021/8/7 11:15
     **/
    @Test
    public void test01()throws Exception{
        String thisTimeStr = "2021-08-07 00:00:00";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = sdf.parse(thisTimeStr);
        //获取 Date 的毫秒数
        long thisTimeLong = date.getTime();
        System.out.println("Date本次的毫秒："+thisTimeLong);

        String lastTimeStr = "2021-08-06 00:00:00";
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime localDateTimeForL = LocalDateTime.parse(lastTimeStr, df);
        //获取 LocalDateTime 的毫秒数
        long lastTimeLong = localDateTimeForL.toInstant(ZoneOffset.of("+8")).toEpochMilli();
        System.out.println("LocalDateTime上次的毫秒："+lastTimeLong);

        long between = DateUtil.between(date,new Date(), DateUnit.DAY);
        System.out.println("时间差："+between);

        if (thisTimeLong < lastTimeLong){
            throw new RuntimeException("本次时间小于上次时间！");
        }
    }
    //格式化 LocalDateTime 使用：DateTimeFormatter
    //将 string 时间转为：LocalDateTime
    public static LocalDateTime getToLocalDateTime(String value){
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return LocalDateTime.parse(value, df);
    }
    @Test
    public void test02()throws Exception{
        String lastTimeStr = "2021-08-06 20:20:20";
        LocalDateTime toLocalDateTime = getToLocalDateTime(lastTimeStr);
        System.out.println(toLocalDateTime);
    }

    /**
     * 将 Date 转换成LocalDateTime
     * atZone()方法返回在指定时区从此Instant生成的ZonedDateTime。
     */
    public static LocalDateTime dateToLocalDateTime(Date date){
        Instant instant = date.toInstant();
        ZoneId zoneId = ZoneId.systemDefault();
        return instant.atZone(zoneId).toLocalDateTime();
    }
    @Test
    public void test03()throws Exception{
        LocalDateTime date = dateToLocalDateTime(new Date());
        LocalDateTime parse = LocalDateTime.parse("2021-08-23 14:57:23.464", df);
        System.out.println(date);
        System.out.println(parse);
    }



}
