package com.cc.testproject.time_date;

import com.cc.testproject.BaseTest;
import org.junit.Test;

import java.time.*;
import java.util.Date;

/**
 * @Description
 * @Author ChenCheng
 * @Date 2021/8/2 10:38
 * @Version 1.0
 */
public class LocalDateTimeTest extends BaseTest {

    //毫秒数、比较、加、减、更改 时间
    @Test
    public void test06()throws Exception{
        LocalDateTime now = LocalDateTime.now();
        long milli = now.atZone(ZoneOffset.of("+8")).toInstant().toEpochMilli();
        System.err.printf("LocalDateTime 毫秒数（东8区）：%s%n",milli);
        System.err.printf("Date 毫秒数（东8区）：%s%n",new Date().getTime());

        LocalDateTime minusYears = now.minusYears(1);
        System.err.printf("比较时间是否相等：%s%n",now.equals(minusYears));

        System.err.printf("减年：%s%n",minusYears);
        System.err.printf("减毫秒：%s%n", now.minusNanos(100000));

        System.err.printf("加周：%s%n", now.plusWeeks(4));
        System.err.printf("加天：%s%n", now.plusDays(7));

        System.err.printf("更改当前时间的年份到2015年：%s%n", now.withYear(2015));
        System.err.printf("更改当前时间的年份到1月份：%s%n", now.withMonth(1));
    }

    //比较两个时间的大小（LocalDateTime）
    @Test
    public void test05()throws Exception{
        LocalDateTime ld1 = LocalDateTime.of(2022, 12, 12, 14, 34, 56);
        LocalDateTime ld2 = LocalDateTime.of(2022, 11, 12, 14, 34, 56);
        System.out.println("ld1在ld2之后" + ld1.isAfter(ld2));
        System.out.println("ld1在ld2之前" + ld1.isBefore(ld2));
    }

    // 获取当前时间
    @Test
    public void test01()throws Exception{
        LocalDateTime now = LocalDateTime.now();
        System.out.println(now);
    }

    //增加时间，特别简单
    @Test
    public void test02()throws Exception{
        LocalDateTime of = LocalDateTime.of(2021, 10, 21, 12, 30, 30);
        System.out.println(of);
    }

    // 将 LocalDateTime 转换成 Date
    public static Date asDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }
    @Test
    public void test03()throws Exception{
        LocalDateTime ldt = LocalDateTime.now();
        Date date = asDate(ldt);
        System.out.println(date);
    }

    /**
     * java8 对时间操作的类还有很多 到java.time包下去看看，以后总会用得到的地方。
     *   Instant：时间戳
     *   Duration：持续时间，时间差
     *   LocalDate：只包含日期，比如：2016-10-20
     *   LocalTime：只包含时间，比如：23:12:10
     *   LocalDateTime：包含日期和时间，比如：2016-10-20 23:14:21
     *   Period：时间段
     *   ZoneOffset：时区偏移量，比如：+8:00
     *   ZonedDateTime：带时区的时间
     *   Clock：时钟，比如获取目前美国纽约的时间。
     */
    @Test
    public void test04()throws Exception{
        //----------Instant：时间戳------------
        Instant now = Instant.now();
        long toEpochMilli = now.toEpochMilli();
        System.out.println("Instant.now():"+toEpochMilli);
        int a = 0;
        for (int i = 0; i < 10000000; i++) {
            a++;
        }
        System.out.println(a);
        long currentTimeMillis = System.currentTimeMillis();
        System.out.println("System.currentTimeMillis():"+currentTimeMillis);
        System.out.println(String.format("耗时：%dms", currentTimeMillis - toEpochMilli));
        //----------Duration：持续时间，时间差------------
        //----------LocalDate：只包含日期，比如：2016-10-20------------
        System.out.println(LocalDate.now());
        //----------LocalTime：只包含时间，比如：23:12:10------------
        System.out.println(LocalTime.now());




    }


}
