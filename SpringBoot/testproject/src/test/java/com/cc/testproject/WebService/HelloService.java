package com.cc.testproject.WebService;

import javax.jws.WebService;

/**
 * 对外发布服务的接口
 */
@WebService
public interface HelloService {
    /**
     * 对外发布服务的接口的方法
     */
    public String getDeviceCurrentData(String loginName,
                                       String loginPassword,
                                       String imei,
                                       Integer pageSize,
                                       Integer pageIndex);
}