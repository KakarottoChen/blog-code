package com.cc.testproject.mybatis_stream;

import cn.hutool.core.util.StrUtil;
import com.cc.testproject.BaseTest;
import com.cc.testproject.mybatis_stream.domain.Emp;
import com.cc.testproject.mybatis_stream.mapper.EmpMapper;
import com.google.common.collect.Lists;
import org.apache.ibatis.cursor.Cursor;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Description
 * @Author CC
 * @Date 2022/1/18
 * @Version 1.0
 */
public class EmpTest extends BaseTest {

    @Resource
    private EmpMapper empMapper;

    /**添加数据
     */
    @Test
    public void test01()throws Exception{
        List<String> names = Lists.newArrayList();
        for (int i = 0; i < 1000000; i++) {
            for (int j = 0; j < 100; j++) {
                names.add(StrUtil.format("名字：{}",i));
            }
            empMapper.insertEmp(names);
        }
    }

    /**普通查询所有
     */
    @Test
    public void test02()throws Exception{
        List<Emp> emps = empMapper.selectAll();
        emps.forEach(emp -> {
            System.out.println(emp);
        });
    }

    /**流式查询
     */
    @Test
    @Transactional
    public void test03()throws Exception{
        Cursor<Emp> scan = empMapper.scan(100);
        scan.forEach(emp -> {
            System.out.println(emp);
        });
    }



}
