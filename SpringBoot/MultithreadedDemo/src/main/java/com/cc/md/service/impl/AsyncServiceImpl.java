package com.cc.md.service.impl;

import com.cc.md.service.IAsyncService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/** 1 @Async：类上，说明整个类中的方法都是异步。必须写我们自己配置的线程池 —— ("myIoThreadPool")
 *  2 @Async：方法上，说明这个方法是异步。不用写我们自己配置的线程池
 * @author CC
 * @since 2023/5/24 0024
 */
@Service
@Async("myIoThreadPool")
public class AsyncServiceImpl implements IAsyncService {

    private static final Logger log = LoggerFactory.getLogger(AsyncServiceImpl.class);

    //类上写了@Async，这里就可以不写了。
    //可以不写 ("myIoThreadPool")。因为在IoThreadPool中开启了异步，说明异步用的就是我们配置的io线程池
    //如果类上面打了 @Async ，这里必须写：("myIoThreadPool")
    @Override
//    @Async
    public void async1(){
        //模仿io流耗时
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        log.info("打印：{}", "异步方法1111！");
    }

    //@Async在类上面，说明这个方法也是异步方法
    @Override
    public void async2(){
        //模仿io流耗时
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        log.info("打印：{}", "异步方法2222！");
    }
}
