package com.cc.ewd.web.controller;

import com.cc.ewd.html.HtmlConstants;
import org.apache.poi.poifs.filesystem.DirectoryEntry;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/** 业务需求：前端使用富文本插件生成带HTML标签的word文档，然后需要下载这个word文档。
 * @author CC
 * @since 2023/4/24 0024
 */
@RestController
@RequestMapping("/apachePoiExport")
public class ApachePoiExport {

    /** 将HTML内容（富文本生成的HTML）转换为Word文档并下载
     * @param response HTTP响应
     */
    @GetMapping
    public void getDoc(HttpServletResponse response) {
        String fileName = "Word文件名";
        String html = HtmlConstants.HTML2;
        //导出word的方法
        exportWord(fileName, html, response);
    }

    /** <p>将HTML内容（富文本生成的HTML）转换为Word文档并下载（word2007之后的_docx）</p>
     *  <li>参考：https://my.oschina.net/u/1045509/blog/1924024</li>
     *  <li>参考：https://blog.csdn.net/qq_42682745/article/details/120867432</li>
     * @param fileName 文件名
     * @param html 富文本生成的HTML
     * @param response 响应
     * @since 2023/4/25 0025
     * @author CC
     **/
    public static void exportWord(String fileName, String html, HttpServletResponse response) {
        //0、获取富文本的html：
        // HTML内容必须被<html><body></body></html>包装；最好设置一下编码格式
        // HTML在这里设置<head></head>是为了让输入的文档是以"页面视图"。而不是"Web版式"
        String wrappedHtml =
                "<html xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\"\n" +
                       "xmlns:w=\"urn:schemas-microsoft-com:office:word\" xmlns:m=\"http://schemas.microsoft.com/office/2004/12/omml\"\n" +
                       "xmlns=\"http://www.w3.org/TR/REC-html40\">" +
                    "<head>" +
                        "<!--[if gte mso 9]><xml><w:WordDocument><w:View>Print</w:View><w:TrackMoves>false</w:TrackMoves>" +
                        "<w:TrackFormatting/><w:ValidateAgainstSchemas/><w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>" +
                        "<w:IgnoreMixedContent>false</w:IgnoreMixedContent><w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>" +
                        "<w:DoNotPromoteQF/><w:LidThemeOther>EN-US</w:LidThemeOther><w:LidThemeAsian>ZH-CN</w:LidThemeAsian>" +
                        "<w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript><w:Compatibility><w:BreakWrappedTables/>" +
                        "<w:SnapToGridInCell/><w:WrapTextWithPunct/><w:UseAsianBreakRules/><w:DontGrowAutofit/><w:SplitPgBreakAndParaMark/>" +
                        "<w:DontVertAlignCellWithSp/><w:DontBreakConstrainedForcedTables/><w:DontVertAlignInTxbx/><w:Word11KerningPairs/>" +
                        "<w:CachedColBalance/><w:UseFELayout/></w:Compatibility><w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel>" +
                        "<m:mathPr><m:mathFont m:val=\"Cambria Math\"/><m:brkBin m:val=\"before\"/><m:brkBinSub m:val=\"--\"/>" +
                        "<m:smallFrac m:val=\"off\"/><m:dispDef/><m:lMargin m:val=\"0\"/> <m:rMargin m:val=\"0\"/><m:defJc m:val=\"centerGroup\"/>" +
                        "<m:wrapIndent m:val=\"1440\"/><m:intLim m:val=\"subSup\"/><m:naryLim m:val=\"undOvr\"/></m:mathPr></w:WordDocument>" +
                        "</xml><![endif]-->" +
                    "</head>" +
                    "<body>%s</body>" +
                "</html>";

        wrappedHtml = String.format(wrappedHtml, html);

        //1、将HTML转换为Word文档byte数组
        byte[] bytes = wrappedHtml.getBytes(StandardCharsets.UTF_8);
        try (POIFSFileSystem poifsFileSystem = new POIFSFileSystem();
             InputStream byteInputStream = new ByteArrayInputStream(bytes);
//             InputStream inputStream = new BufferedInputStream(byteInputStream);
             ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ){
            //2、使用ApachePoi转换word并设置到输出流outputStream
            DirectoryEntry directory = poifsFileSystem.getRoot();
            //WordDocument名称不允许修改
            directory.createDocument("WordDocument", byteInputStream);
            //将Word文档写入POIFSFileSystem对象
            poifsFileSystem.writeFilesystem(outputStream);

            //3、①将Word文档（输出流outputStream）写入HTTP响应并下载；②也可以上传到自己的文件服务器然后返回URL给前端下载。
            response.setCharacterEncoding("utf-8");
            //设置content-type就是告诉浏览器这是啥玩意儿
            //"octet-stream" ：通用二进制流；
            //"msword" ：Microsoft Word文档
            //"vnd.openxmlformats-officedocument.wordprocessingml.document" ：响应的内容类型设置为Microsoft Word 2007及更高版本的docx格式。对应的文件名后缀需要改成”docx“
            response.setContentType("application/vnd.openxmlformats-officedocument.wordprocessingml.document;charset=UTF-8");
            //解决跨域不显示在header里面的问题
            response.setHeader("Access-Control-Expose-Headers","Content-disposition");
            //"attachment"：让浏览器把响应视为附件并下载
            //"inline"：    让浏览器打开Word文档而不是下载它
            response.setHeader("Content-disposition","attachment; filename=" +
                    URLEncoder.encode(fileName.concat(".docx"), "UTF-8"));
            //BufferedOutputStream缓冲流：可以将数据缓存在内存中，以减少对底层IO的调用次数，从而提高性能。
            //ServletOutputStream：用于向客户端发送数据的
            //因为需要将数据写入HTTP响应，所以使用ServletOutputStream是更好的选择。
            OutputStream out = new BufferedOutputStream(response.getOutputStream());
            out.write(outputStream.toByteArray());
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /** 方式一、方式二是等价的
     * <li>推荐使用方式一</li>
     * @since 2023/5/18 0018
     * @author CC
     **/
    public void closeStream() throws IOException {
        //使用方式一（推荐）：各种“流”，这些流都是需要关闭的。
        // -> 这些流会自动关闭。这些流的作用域仅在这个try代码块中
        try (POIFSFileSystem poifsFileSystem = new POIFSFileSystem();
             InputStream byteInputStream = new ByteArrayInputStream(new byte[1]);
             ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ){
            //业务……使用：poifsFileSystem、byteInputStream、outputStream
        } catch (Exception e) {
            e.printStackTrace();
        }

        //使用方式二：各种“流”，这些流都是需要关闭的。
        // -> 这些流需要手动关闭
        POIFSFileSystem poifsFileSystem = new POIFSFileSystem();
        InputStream byteInputStream = new ByteArrayInputStream(new byte[1]);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            //业务……使用：poifsFileSystem、byteInputStream、outputStream
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            poifsFileSystem.close();
            byteInputStream.close();
            outputStream.close();
        }
    }

}
