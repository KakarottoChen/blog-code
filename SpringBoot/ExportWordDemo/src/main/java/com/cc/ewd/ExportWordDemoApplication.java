package com.cc.ewd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExportWordDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExportWordDemoApplication.class, args);
    }

}
