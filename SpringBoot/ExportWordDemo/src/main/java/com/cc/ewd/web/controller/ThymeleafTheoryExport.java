package com.cc.ewd.web.controller;

import com.cc.ewd.vo.Msg4Vo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** thymeleaf导出的原理
 * @author CC
 * @since 2023/4/25 0025
 */
@RestController
@RequestMapping("/thymeleafTheoryExport")
public class ThymeleafTheoryExport {

    @Resource
    private SpringTemplateEngine springTemplateEngine;

    /** <p>原理</p>
     * <ol>
     *     <li>相当于把word文件转为xml或者html，然后修改其中的值再以xml、html下载成word文件</li>
     *     <li>这个方法只能运行一次，因为对ClassLoaderTemplateResolver的设置是一次性的</li>
     *     <li>所以需要将ClassLoaderTemplateResolver设置成单例：配置Bean。</li>
     *     <li>doc或docx的模板别使用WPS的文档，使用微软的office新建word文档，然后转为xml或html</li>
     *     <li>可以导出xml、也可以导出html：建议使用xml</li>
     * </ol>
     */
    @GetMapping
    public void thymeleafExport(HttpServletResponse response){
        String fileName = "第一个thy的文件";

        //一、设置Thymeleaf模板
        ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        //xml文件地址：自定义xml的文件夹：thymeleafcs/thymeleaf_1_wps.xml
        //xml文件地址：默认放在thymeleaf下就可以读取到
        templateResolver.setPrefix("thymeleafcs/");
        //设置文件的后缀
        templateResolver.setSuffix(".xml");
//        templateResolver.setSuffix(".html");
        templateResolver.setCharacterEncoding("utf-8");
        //模板模式：默认是HTML。改为xml
        templateResolver.setTemplateMode(TemplateMode.XML);
//        templateResolver.setTemplateMode(TemplateMode.HTML);
        //加载模板
        springTemplateEngine.setTemplateResolver(templateResolver);
        //启用Spring EL编译器
        springTemplateEngine.setEnableSpringELCompiler(true);

        //二、设置数据（可以用map，也可以用对象）
        Map<String,Object> map = new HashMap<>();
        //1普通文本参数
        map.put("msg1","我是参数1111");
        map.put("msg2","我是参数2222");
        map.put("msg3","我是参数3333");
        //2if-else参数
        map.put("thIf1","1");
        map.put("thIf2","2");

        //3循环：构建集合参数，用于表格：可以是Map；可以是对象
//        List<Map<String,Object>> msg4Vos = new ArrayList<>();
        List<Msg4Vo> msg4Vos = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            //1map方式
//            Map<String,Object> map4 = new HashMap<>();
//            map4.put("l1","列1-" + i);
//            map4.put("l2","列2-" + i);
//            map4.put("l3","列3-" + i);
//            map4.put("l4","列4-" + i);
//            msg4Vos.add(map4);
            //2对象方式
            Msg4Vo vo = new Msg4Vo();
            vo.setL1("列1-" + i);
            vo.setL2("列2-" + i);
            vo.setL3("列3-" + i);
            vo.setL4("列4-" + i);
            msg4Vos.add(vo);
        }
        map.put("msg4Vos",msg4Vos);

        //4设置数据
        Context context = new Context();
        context.setVariables(map);
        //写入输入（模板名称，数据）
        String process = springTemplateEngine.process("thymeleaf_4_wps_final", context);

        //三、下载
        //建议下载成doc的。不然微软的office可能打不开
        try {
            byte[] bytes = process.getBytes(StandardCharsets.UTF_8);
//            ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
//            ByteArrayOutputStream outputStream = getByteArrayOutputStream(inputStream);

            response.setCharacterEncoding("utf-8");
            response.setContentType("application/msword");
            response.setHeader("Access-Control-Expose-Headers","Content-disposition");
            response.setHeader("Content-disposition","attachment; filename=" +
                    URLEncoder.encode(fileName.concat(".doc"), "UTF-8"));
            ServletOutputStream out = response.getOutputStream();

            //两种方式都可以：用bytes好些
//            out.write(outputStream.toByteArray());
            out.write(bytes);
            out.flush();
            out.close();
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    /** 将 ByteArrayInputStream 拷贝成 ByteArrayOutputStream
     *  将 字节数组输入流 拷贝成 字节数组输出流
     */
    public static ByteArrayOutputStream getByteArrayOutputStream(ByteArrayInputStream inputStream) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int length;
        while ((length = inputStream.read(buffer)) != -1) {
            outputStream.write(buffer, 0, length);
        }
        return outputStream;
    }

}
