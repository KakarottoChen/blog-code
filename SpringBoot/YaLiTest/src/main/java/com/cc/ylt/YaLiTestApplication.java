package com.cc.ylt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YaLiTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(YaLiTestApplication.class, args);
    }

}
