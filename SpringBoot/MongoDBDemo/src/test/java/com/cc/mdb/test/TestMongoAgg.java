package com.cc.mdb.test;

import com.alibaba.fastjson.JSONObject;
import com.cc.mdb.BaseTest;
import org.junit.Test;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.data.mongodb.core.query.Criteria;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * 聚合查询
 *      $project	        指定返回字段
 *      $lookup	            多表关联
 *      $unwind	            数组拆分
 *      $match	            筛选条件
 *      $sort	            排序
 *      $limit	            分页
 *      $skip	            跳过
 *      $group	            根据id(可自定义)分组
 *      $sum	            分组后求和
 *      $first	            分组后取第一条
 *      $last	            分组后取最后一条
 *      $addToSet	        分组后添加到list
 *      $cond	            可以做 if-else
 * @author CC
 * @since 2023/4/20 0020
 */
public class TestMongoAgg extends BaseTest {

    @Resource
    private MongoTemplate mongoTemplate;

    private static final String COLLECTION_NAME = "USER";

    /** 分组
     */
    @Test
    public void test00()throws Exception{
        Aggregation groupAgg = Aggregation.newAggregation(
                Aggregation.group("age") // 分组条件
                        .first("age").as("age_R")     // 获取分组后查询到的该字段的第一个值
//                        .first("school.sId").as("sId_R")
                        .sum("age").as("sum_age_R")  // 按分组对该字段求和
                        .avg("age").as("avg_age_R")  // 按分组求该字段平均值
                        .sum("id").as("sum_id_R")    // 按age分组后求id的和
                        .min("id").as("min_id_R")    // 按age分组后求id的最小值
                        .max("id").as("max_id_R")    // 按age分组后求id的最大值
        );

        AggregationResults<JSONObject> results = mongoTemplate.aggregate(groupAgg, COLLECTION_NAME, JSONObject.class);
        List<JSONObject> res = results.getMappedResults();
        System.out.println(res);


    }

    /** 聚合查询：数组拆分 + 指定返回字段 + 排序 + 分页
     */
    @Test
    public void testAgg01()throws Exception{
        // 1. 配置聚合查询
        // 1.1. 将school字段分解成数组
        UnwindOperation unwind = Aggregation.unwind("school");
        // 1.2. 获取school.sId的属性中大于2的对象
        MatchOperation match = Aggregation.match(Criteria.where("school.sId").gt(2));
        // 1.3. 显示的字段
        ProjectionOperation project = Aggregation.project("_id", "school.sId",
                "school.sName", "name", "id", "age", "email");

        // 1.4. 查询所有
        Aggregation aggregation = Aggregation.newAggregation(
                unwind,
                match,
                project
        );
        // 1.5. 查询数量：把数量封装在totalCount这个值中
        Aggregation countAgg = Aggregation.newAggregation(
                unwind,
                match,
                Aggregation.count().as("totalCount")
        );
        // 1.6. 排序+分页
        Aggregation sortAndSkipLimitAgg = Aggregation.newAggregation(
                unwind,
                match,
                project,
                // 1.6.1. 排序：先按照sId升序，再按照id降序
                Aggregation.sort(Sort.Direction.ASC,"sId")
                        .and(Sort.Direction.DESC,"id"),
                // 1.6.2. 分页
                Aggregation.skip(0L),
                Aggregation.limit(2L)
        );

        // 2. 查询
        // 2.1. 查询全部
        AggregationResults<JSONObject> results = mongoTemplate.aggregate(aggregation, COLLECTION_NAME, JSONObject.class);
        // 2.1.1. 获取映射结果
        List<JSONObject> allResult = results.getMappedResults();
        allResult.forEach(System.out::println);
        // 2.2. 查询数量
        AggregationResults<JSONObject> resultCount = mongoTemplate.aggregate(countAgg, COLLECTION_NAME, JSONObject.class);
        JSONObject jsonObject = resultCount.getUniqueMappedResult();
        System.out.println(jsonObject);
        Object totalCount = jsonObject.get("totalCount");
        System.out.println("总数：" + totalCount);

        // 2.3. 排序+分页查询
        AggregationResults<JSONObject> sortSkip = mongoTemplate.aggregate(sortAndSkipLimitAgg, COLLECTION_NAME, JSONObject.class);
        List<JSONObject> mappedResults = sortSkip.getMappedResults();
        System.out.println(mappedResults);

        //获取原始结果
//        Document rawResults = results.getRawResults();
        //获取唯一映射结果（只获取一个值）
//        JSONObject uniqueMappedResult = results.getUniqueMappedResult();
        //获取服务器使用情况
//        String serverUsed = results.getServerUsed();
        //循环：和mappedResults循环一样的
//        results.forEach(result -> {
//            System.out.println(result);
//        });
    }

    //添加聚合查询的数据
    @Test
    public void testAgg02() throws Exception {
        //写入复杂数据 —— 有子文档（json数据中有下级）
        List<JSONObject> vos = new ArrayList<>();
        for (int i = 1; i < 20; i++) {
            JSONObject result = new JSONObject();
            String aa = i + "" + i + "" + i + "";
            result.put("id", aa);
            result.put("name", "cscs" + aa);
            int age = 123;
            if (i >= 10){
                age = 345;
            }
            result.put("age", age);
            result.put("email", "11@qq.com");

            List<JSONObject> jss = new ArrayList<>();
            if (i < 6){
                for (int j = 1; j < 3; j++) {
                    JSONObject js = new JSONObject();
                    js.put("sId", j * i);
                    js.put("sName", "学校" + aa);
                    js.put("sNo", "学校编号" + aa);
                    js.put("sTab", j * i);
                    jss.add(js);
                }
            }
            result.put("school", jss);
            vos.add(result);
        }
        Collection<JSONObject> user3 = mongoTemplate.insert(vos, COLLECTION_NAME);
        System.out.println(user3);
    }


}
